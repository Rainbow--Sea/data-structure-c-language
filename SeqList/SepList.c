#define  _CRT_SECURE_NO_WARNINGS  1

#include"SeqList.h"


/*
* 静态的顺序表的构建
* 1. 使用定长数的存储
* 2. 问题：
*        给少了不够用，给够了用不完浪费，不能灵活的控制，
         而且内存的使用十分的有限的，栈帧，并不能给太大的
*/

/*
* 
* 静态的顺序表
void SeqListInit(SL* sl) // 初始化顺序表
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		sl->data[i] = 0;
	}

	
	 // 也可以使用函数 mexmset  头文件是 ：#include<string.h>
	 // mextmset(pa->data, 0,sizeof(SQDataType)*MAX_SIZe ); 
	 // 将data 数组初始化为 0；
	
	sl->size = 0;

}

void SeqListPushBack(SL*sl,SQDataType x )  // 顺序表的尾插法
{
	if (sl->size == MAX_SIZE) // 1.首先判断是否有空间可以插入
	{
		printf("顺序表已经满了，无法再进行插入操作了\n");
		perror("It is full"); // 打印错误；
		exit(0); // 程序停止运行，
	}
	sl->data[sl->size] = x; // 插入数据 x ;
	sl->size++;

}
*/



/*
* 动态顺序表
*/

void SeqListInit(SL* ps)    // 初始化顺序表
{
	/*ps->Data = (SQDataType*)malloc(2*sizeof(SQDataType));*/
	ps->Data = NULL;
	ps->size = 0;
	ps->capacity = 0;
}



static void SeqListCheckCapacity(SL*ps) // 检查是否动态顺序是否满了，并扩容，static修饰函数，
{                                       //  是该函数只能在本文件中使用，外部文件无法直接访问，但是可以被间接访问（函数接口）
	                                    // 封装函数，提高项目的安全性，减少后期的维护成本

	// 一般扩容的大小为，原来的 2 倍，如果太小了，你每次都会需要增容的
	// 如果太大了，你就会太浪费空间了，所以大小要适宜，不可太大，太小
	/* 
	* 还有一个注意的地方：
	* 因为你一开始的容量初始化的是 0 ，因为 realloc 函数的一些性质单位字节，开辟一个0 字节空间的大小返回的是NULL
	* 所以这里当 capacity 容量为 0 ，我们赋值为 4 
	*/
	if (ps->size == ps->capacity)    // 检查是否动态顺序表是否满了
	{
		
		int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		SQDataType* ptr = (SQDataType*)realloc( ps->Data, newcapacity * sizeof(SQDataType));
		if (ptr == NULL)          // 判断动态内存（堆区）开辟空间是否成功
		{
			perror("ptr");        // 打印错误原因
			free(ptr);            // 开辟空间失败，手动释放空间，
			ptr = NULL;           // 开辟空间失败，防止非法访问(因为该空间已经释放了，不属于你的了，你再想通过该指针访问的
			exit(0);              //  话，就是越界，非法访问了 )，所以置为NULL;
			// exit(0);空间开辟失败，程序停止运行，
		}
		else                      // 开辟（堆区）空间开辟成功
		{
			ps->Data = ptr;       // 交付控制权
			ps->capacity = newcapacity;
		}

	}

}



void SeqListPushBack(SL* ps, SQDataType x)     // 动态顺序表的尾插法
{
	SeqListCheckCapacity(ps);                  // 判断尾插时，空间是否满了
	ps->Data[ps->size] = x;
	ps->size++;

}



void SeqListPushFront(SL* ps, SQDataType x)    // 动态顺序表的头插法 
{
	SeqListCheckCapacity(ps);                  // 判断头插法，空间是否满了
	int end = ps->size - 1;                    // 实际存放有数据的下标位置
	while (end >= 0)
	{
		ps->Data[end + 1] = ps->Data[end];
		end--;

	}
	ps->Data[0] = x;
	ps->size++;
	
}



// 顺序表的尾删
void SeqListPopBack(SL* ps)     // 动态顺序表的尾删
{
	if (ps->size == 0)          // 判断是否有数据的存在可以删除的
	{
		printf("没有数据可以删除，失败\n");
		exit(0);                // 删除失败， 退出程序
	}
	ps->size--;                 // 直接把保存的有效数减减即可
}



void SeqListPopFront(SL* ps)    // 动态顺序表的头删
{
	if (ps->size == 0)          // 首先判断是否存在可以删除的数据
	{
		printf("没有可以删除的数据，所以删除失败\n"); 
		exit(0);                // 删除失败，程序停止运行
	}

	int start = 1;
	while (start < ps->size)
	{
		ps->Data[start -1] = ps->Data[start];
		start++;
	}
	ps->size--;
}



void SeqListInsert(SL* ps, int pos, SQDataType x)  // 动态顺序表在pos的位置插入数据
{
	if (pos < 0 || pos > ps->size)                 // 判断插入的数据是否存在插入位置的错误
	{
		perror("SeqListInsert");                   // 打印错误原因
		exit(0);                                   // 插入位置错误，程序退出
	}
	SeqListCheckCapacity(ps);
	
	int end = ps->size - 1;                        // 记入的是数据的有效位置的下标
	while (end >= pos)                             // 所有数据后移动，腾出位置给 pos插入
	{
		ps->Data[end + 1] = ps->Data[end];
		end--;
	}
	ps->Data[pos] = x;
	ps->size++;

}



void SeqListErase(SL* ps, int pos)                 // 动态顺序表删除 pos 位置的值
{
	if (pos < 0 || pos > ps->size)                 // 判断删除的位置是否正确
	{
		perror("SeqListErase");                    // 打印错误原因
		exit(0);                                   // 退出程序
	}
	int start = pos + 1;                           // 把删除的位置的后一位
	while (start < ps->size)                       // 所有数据往前移动，覆盖
	{
		ps->Data[start -1] = ps->Data[start];
		start++;
	}
	ps->size--;                

}



void  SeqListFind(SL* ps, SQDataType x)             // 查找 x 值，的下标
{
	int sign = 1;                                   // 标记判断
	for (int i = 0; i < ps->size; i++)
	{
		if (x == ps->Data[i])
		{
			sign = 0;
			printf("数值 %d 的位置是在下标为:%d的位置\n", x, i);
			break;
		}
			
	}
	if (sign == 1)
	{
		printf("数值 %d 不存在\n", x);
	}
	
}



void SeqListModity(SL* ps, int pos, SQDataType x)    // 修改动态顺序表的 pos 下标位置的数值
{
	if (pos < 0 || pos >ps->size)                    // 判断修改的位置是否合理
	{
		perror("SeqListModity");                     // 打印错误原因
		exit(0);                                     // 修改失败，退出程序
	}
	ps->Data[pos] = x;
}



void SeqListPrint(SL* ps)                            // 动态顺序表的打印
{
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d\t", ps->Data[i]);
	}
	printf("\n");
}



void SeqListDestay(SL* ps)                          // 动态顺序表的销毁
{
	free(ps->Data);                                 // 释放动态（堆区）开辟的空间
	ps->Data = NULL;                                // 置为 NULL，防止越界访问，以及非法访问
	ps->capacity = 0;
	ps->size = 0;

}

void menu()
{
	printf("******************************************\n");
	printf(" 1.打印数据		2.尾插		   \n");
	printf(" 3.头   插		4.头删		   \n");
	printf(" 5.尾   删		6.pos位置删除   \n");
	printf(" 7.pos位置插入	8.pos查找	   \n");
	printf(" 9.修改数据		0.销毁		   \n");
	printf("******************************************\n");
}
