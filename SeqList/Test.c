#define  _CRT_SECURE_NO_WARNINGS  1

#include"SeqList.h"



int main()
{
	//  SL sl;                  // 定义一个静态的顺序表的结构类型变量
	/*
	* 测试，调式
	SeqListInit(&ps);           // 初始化线性表
	SeqListPushBack(&ps, 1);
	SeqListPushBack(&ps, 2);
	SeqListPushBack(&ps, 3);
	SeqListPushBack(&ps, 4);
	SeqListPushBack(&ps, 5);
	SeqListPushBack(&ps, 6);
	SeqListPushBack(&ps, 7);
	SeqListPushBack(&ps, 8);
	SeqListPushBack(&ps, 9);
	SeqListPushBack(&ps, 10);
	SeqListPushBack(&ps, 11);
	SeqListPushBack(&ps, 12);
	SeqListPrint(&ps);
	SeqListPushFront(&ps, 0);
	SeqListPrint(&ps);
	SeqListPushBack(&ps, 100);
	SeqListPrint(&ps);
	SeqListPopBack(&ps);         // 动态顺序表的尾删
	SeqListPrint(&ps);
	SeqListPopFront(&ps);        // 动态顺序表的头删
	SeqListPrint(&ps);
	SeqListInsert(&ps, 0, 999); 
	SeqListPrint(&ps);
	SeqListErase(&ps, 1);        // 动态顺序表删除 pos 位置的值
	SeqListPrint(&ps);
	SeqListFind(&ps, 4);         // 查找 x 值，的下标
	SeqListModity(&ps, 3, 9);    // 修改动态顺序表的 pos 下标位置的数值
	SeqListPrint(&ps);
	SeqListDestay(&ps);          // 动态顺序表的销毁
	*/

	SL ps;                      // 定义一个动态的顺序表的结构类型变量
	int option = 0;
	int sign = 0;
	int num = 0;
	SeqListInit(&ps);           // 初始化线性表
	do
	{
		menu();
		printf("请选择功能：");
		scanf("%d", &option);
		system("cls");          // 刷新控制台
		switch (option)
		{
		case DESTROY :
			SeqListDestay(&ps);           // 动态顺序表的销毁
			break;
		case PRINT :
			SeqListPrint(&ps);           // 动态顺序表打印
			break;
		case ENDINSERT :                 // 动态顺序表尾插
			printf("请输入尾插的数值：");
			scanf("%d", &sign);
			SeqListPushBack(&ps, sign);
			break;
		case HEADINSERT :                // 动态顺序表头插
			printf("请输入头插的数值：");
			scanf("%d", &sign);
			SeqListPushFront(&ps, sign);
			break;
		case HEADNEIETE:                 // 动态顺序表头删 
			SeqListPopFront(&ps);
			break;
		case ENDDELETE:                  // 动态顺序表尾删 
			SeqListPopBack(&ps);         
			break;
		case POSDDIETE:                  // 动态顺序表pos删 
			printf("请输入需要删除的的下标：");
			scanf("%d", &sign);
			SeqListErase(&ps, sign);
			break;
		case POSINSERT:                  // 动态顺序表pos插
			printf("请输入需要插入的的下标：");
			scanf("%d", &sign);
			printf("\n");
			printf("请输入需要插入的的数值：");
			scanf("%d", &num);
			SeqListInsert(&ps, sign, num);
			break;
		case POSCHECK :                     // 动态顺序表pos查找
			printf("请输入需要查找的数值：");
			scanf("%d", &sign);
			SeqListFind(&ps, sign);      
			break;
		case POSAMEND:                      // 动态顺序表pos修改
			printf("请输入需要修改的对应的下标：");
			scanf("%d", &sign);
			printf("\n");
			printf("请输入需要修改的对应的数值：");
			scanf("%d", &num);
			SeqListModity(&ps, sign, num);
			break;
		default :
			printf("请不要输入无关的数值\n");
			break;
		}
		
		
	} while (option != 0);

	return 0;
}

