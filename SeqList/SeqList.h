#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<windows.h>

/*静态顺序表
#define MAX_SIZE 10 // 顺序表的固定容量
typedef int SQDataType; // 对 int 类型的别名，方便后面的数据类型的修改

// 静态的顺序表 （结构体）
typedef struct SeqList
{
	SQDataType data[MAX_SIZE]; // 定义数组
	int size;                  // 计入数据的有效个数
}SL;

extern void SeqListInit(SL* sl);  // 初始化静态的顺序表 ,extern 声明加上它，代码好风格
extern void SeqListPushBack(SL* sl,SQDataType x); // 静态的顺序表的尾插法;  // 顺序表的尾插法，extern 声明 ，代码好风格
*/
/**************************************************************************/


/*
* 动态的顺序表
*/

typedef int SQDataType;    // 对 int 类型的别名，方便后面的数据类型的修改

typedef struct SeqList
{
	SQDataType* Data;      // 指向动态（堆区）开辟空间的数组
	int size;              // 有效数据的个数
	int capacity;          // 容量空间的大小


}SL;

enum Function           // 枚举
{
	DESTROY=0,          // 销毁
	PRINT,              // 打印
	ENDINSERT,          // 尾插
	HEADINSERT,         // 头插
	HEADNEIETE,         // 头删         // 注意：枚举一般是大写，枚举是逗号，枚举的最后一个不用加逗号，
	ENDDELETE,          // 尾删
	POSDDIETE,          // pos删
	POSINSERT,          // pos插
	POSCHECK,           // pos查
	POSAMEND            // pos改

};

extern void menu();                                          // 打印菜单；

extern void SeqListInit(SL* ps);                             // 初始化动态的顺序表 ,extern 声明加上它，代码好风格
extern void SeqListPushBack(SL* ps,SQDataType x);            // 动态的顺序表的尾插法，extern 声明 ，代码好风格
extern void SeqListPushFront(SL* ps, SQDataType x);          // 动态顺序表的头插法 extern 声明，代码好风格
extern void SeqListPrint(SL* ps);                            // 动态顺序表的打印
extern void SeqListPopBack(SL* ps);                          // 动态顺序表的尾删
extern void SeqListPopFront(SL* ps);                         // 动态顺序表的头删
extern void SeqListInsert(SL* ps, int pos, SQDataType x);    // 动态顺序表在pos的位置插入数据
extern void SeqListErase(SL* ps, int pos);                   // 动态顺序表删除 pos 位置的值
extern void SeqListFind(SL* ps, SQDataType x);               // 查找 x 值，的下标
extern void SeqListModity(SL* ps, int pos, SQDataType x);    // 修改动态顺序表的 pos 下标位置的数值
extern void SeqListDestay(SL* ps);                           // 动态顺序表的销毁






