#define  _CRT_SECURE_NO_WARNINGS  1

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

// 打印数组
void playPrint(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}

	printf("\n");
}



// 交换数值
void swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}



// 冒泡排序，降序
void bubbleSortDesc(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		int exchange = 0; // 标记

		for (int j = 1; j < n - i; j++)
		{
			exchange = 1;

			if (arr[j - 1] < arr[j])    // 降序,将小的放到后面
			{
				swap(&arr[j - 1], &arr[j]);   // 交换数值
			}
		}

		// 一次内循环，都没有进行了,表示已经有序了，停止循环排序
		if (0 == exchange)
		{
			break;
		}
	}
}


// 冒泡排序，升序
void bubbleSortAsc(int* arr,int n )
{
	for (int i = 0; i < n; i++)
	{
		int exchange = 0;      // 标记

		// j = 1从下标 1 开始, j < n-i; 每一次循环都会把最后一个数值排序好,所以需要n-i 依次减少排序次数
		for (int j = 1; j < n-i; j++)       // 因为后面的都排序好了,就不要动了。
		{
			exchange = 1;      // 更新标记

			if (arr[j - 1] > arr[j])     // 这里从 0 开始,升序,把大的放到后面
			{
				swap(&arr[j - 1], &arr[j]);   // 交换数值
			}
		}

		// 一趟也没有，交换，则已经有序了，不用排序了，退出循环
		if (0 == exchange)
		{
			break;
		}

	}
}



// 冒泡排序，升序，另外一种方式
void bubbleSortAsc2(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		int exchange = 0;     // 标记

		// j < n-i-1; 多减一个1 是为了防止 j +1 越界,升序,当最大值在前面时，j = 0; j < n在 j +1 可能就越界了
		for (int j = 0; j < n - i - 1; j++)                 // 所以需要多减一个 -1 
		{    // j< n-i-1 每一次循环都会，把最后一个数值排序好,所以 j < n-i-1 减少循环次数
			exchange = 1;       // 标记

			if (arr[j] > arr[j + 1])    // 交换,后移动
			{
				swap(&arr[j], &arr[j + 1]);
			}
		}
	}
}





// 冒泡排序测试
void testBubbleSort()
{
	int arr[] = { 1,6,8,4,10,2,9,7,3,5,0 };

	bubbleSortDesc(arr, sizeof(arr) / sizeof(int));
	// bubbleSortAsc2(arr, sizeof(arr) / sizeof(int));
	// bubbleSortAsc(arr, sizeof(arr) / sizeof(int));
	playPrint(arr, sizeof(arr) / sizeof(int)); 

	/* 
	* sizeof(arr)/sizeof(int) 表示数组的长度
	* sizeof(arr) 表示数组的所占字节大小
	* sizeof(int）表示数组中元素类型所占的字节大小
	*/
}


// 冒泡排序，性能测试
void effciencyTest()
{
	srand(time(0));     // 时间种子

	const int N = 1000000;     // 定义 10W

	int* arr = (int*)malloc(sizeof(int) * N);   // 堆区上开辟是 10 w的动态空间

	// 判断动态内存(堆区)开辟的空间是否，成功
	if (NULL == arr)
	{
		perror("malloc error");   // 错误提醒
		exit(-1);           // 非正常退出程序
	}

	for (int i = 0; i < N; i++)
	{
		arr[i] = rand();
	}

	int begin = clock();      // 冒泡排序前的时间点，单位：毫秒
	bubbleSortAsc(arr, N);    // 冒泡排序,升序
	int end = clock();        // 冒泡排序结束后的时间点, 单位：毫秒

	/* 
	排序效率：=  排序结束的时间点 - 排序前的时间点
	*/

	printf("冒泡排序: %d\n", end - begin);

}


int main()
{
	// 冒泡排序，测试
//	testBubbleSort();

	// 效率测试
	effciencyTest();    
	return 0;
}