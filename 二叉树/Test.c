

#include"BinaryTree.h"

// int size1 = 0;   // 计算二叉树节点的方式一，全局变量，存在累计问题
int main()
{
	test();

	return 0;
}


void test()
{
	BTNode* A = NULL;
	BTNode* B = NULL;
	BTNode* C = NULL;
	BTNode* D = NULL;
	BTNode* E = NULL;


	A = BinaryTreeGenerate('A');  // 创建生成二叉树节点
	B = BinaryTreeGenerate('B');
	C = BinaryTreeGenerate('C');
	D = BinaryTreeGenerate('D');
	E = BinaryTreeGenerate('E');

	A->left = B;      // A的左子树
	A->right = C;     // A的右子树
	B->left = D;      // B的左子树
	B->right = E;     // B的右子树

	printf("前序遍历: ");
	PrerOrder(A);     // 前序遍历
	printf("\n");   
	printf("中序遍历: ");
	InOrder(A);       // 中序遍历
	printf("\n");
	printf("后序遍历: ");
	PostOrder(A);     // 后序遍历

	/*
	* 使用方式一：定义全局变量，计算二叉树节点存在累积问题
	TreeSize1(A);
	printf("\n");
	printf("%d\n", size1); // 5
	TreeSize1(B); 
	printf("%d\n", size1); // 8 这里size 累计到了上次的size的数值了
	*/

	/*使用方式二：传实参的地址，形参的改变影响实参
	int size = 0;
	printf("\n");
	TreeSize2(A, &size);
	printf("%d\n", size);
	int num = 0;
	TreeSize2(B, &num);
	printf("%d\n", num);
	*/

	printf("\n以A为根节点的二叉树中的所有节点的个数：%d",TreeSize(A));    // 计算二叉树中所有节点的个数
	printf("\n以B为根节点的二叉树中的所有节点的个数：%d\n",TreeSize(B));    // 计算二叉树中所有节点的个数
	printf("以A为根节点的二叉树中的所有叶子节点个数：%d", TreeLeafSize(A));   // 计算二叉树中所有叶子节点的个数
	printf("\n");
	printf("以B为根节点的二叉树中的所有叶子节点个数：%d", TreeLeafSize(B));   // 计算二叉树中所有叶子节点的个数
	printf("\n");
	printf("以C为根节点的二叉树中的所有叶子节点个数：%d", TreeLeafSize(C));   // 计算二叉树中所有叶子节点的个数


	DestroryTree(A);      // 清空二叉树中的节点

}