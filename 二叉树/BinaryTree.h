#pragma once
#define  _CRT_SECURE_NO_WARNINGS  1

#include<stdio.h>
#include<stdlib.h>

typedef char BTDateType;    // 定义存放二叉树中的数据类型

typedef struct BinaryTreeNode {    // 定义二叉树结构体
	struct BinaryTreeNode* left;   // 左子树的
	struct BinaryTreeNode* right;  // 右子树
	BTDateType data;               // 存放的数据

}BTNode;


// extern int size1 ; 存在累计问题

extern void test();        // 用于测试二叉树
extern void BinaryTreeInit(BTNode* root);     // 初始化二叉树
extern BTNode* BinaryTreeGenerate(BTDateType x);   // 产生树节点
extern void PrerOrder(BTNode* root);   // 前序遍历
extern void InOrder(BTNode* root);     // 中序遍历
extern void PostOrder(BTNode* root);   // 后序遍历
// extern void TreeSize1(BTNode* root);              // 计算二叉树的节点个数,方式一：全局变量
// extern void TreeSize2(BTNode* root, int* size);   // 计算二叉树的节点的个数，方式二：传参数，注意时传的是地址
                                                     // 因为形参的改变不会影响到实参的，传地址
extern int TreeSize(BTNode* root);       // 计算二叉树中第三中方式，递归计算
extern int TreeLeafSize(BTNode* root);   // 计算二叉树中叶子节点的个数
extern void DestroryTree(BTNode* root);  // 清空二叉树中的节点