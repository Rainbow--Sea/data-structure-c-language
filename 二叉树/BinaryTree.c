
#include"BinaryTree.h"


// 初始化二叉树的
void BinaryTreeInit(BTNode* root)
{
	root->left = NULL; 
	root->right = NULL;
}



// 生成二叉树节点
BTNode* BinaryTreeGenerate(BTDateType x)
{
	// 内存堆区开辟空间
	BTNode* new = (BTNode*)malloc(sizeof(BTNode));
	// 判断该空间是否创建成功
	if (NULL == new)
	{
		perror("malloc error");   // 打印提示错误
		exit(-1);
	}

	new->left = NULL;     // 左子树节点
	new->right = NULL;    // 右子树节点
	new->data = x;        // 存放的数值

	return new;
}


//  前序遍历
// 将每个节点看作是 根左右，来递归
void PrerOrder(BTNode* root)
{
	if (NULL == root)     // 递归结束条件
	{
		printf("NULL ");
		return;           // 返回到调用它的位置
	}

	printf("%c ", root->data);  // 取根
	PrerOrder(root->left);      // 左子树递归，
	PrerOrder(root->right);     // 右子树递归

}



// 中序遍历
// 把每个节点看作是 根左右，进行递归
void InOrder(BTNode* root)
{
	if (NULL == root)    // 递归结束条件
	{
		printf("NULL ");
		return;          // 递归，返回到调用该函数的位置
	}

	InOrder(root->left);        // 左子树，递归
	printf("%c ", root->data);  // 取根
	InOrder(root->right);       // 右子树，递归

}


// 后序遍历
// 将每个节点看作是 根左右
void PostOrder(BTNode* root)
{
	if (NULL == root)   // 递归结束条件
	{
		printf("NULL ");
		return;         // 递归，返回调用它的位置处
	}
	else
	{
		PostOrder(root->left);      // 左子树递归
		PostOrder(root->right);     // 右子树，递归
		printf("%c ", root->data);  // 取 根
		
	}
}



// 计算二叉树中所有节点的个数，方式三
/* 递归，计算，把每个节点看作是 根左右，计算每个节点的数量*/
int TreeSize(BTNode* root)
{
	if (NULL == root)  // 递归结束条件
	{
		return 0;      // 当左右树节点为 NULL,返回 0，返回调用它的位置
	}
	else
	{
		return TreeSize(root->left) + TreeSize(root->right) + 1;
		// 把其节点的左右子树的节点个数加起来
	}

}



// 计算二叉树中叶子节点的个数 注意把每个节点看成是 根左右的树
int TreeLeafSize(BTNode* root)
{
	if (NULL == root)   // 递归结束条件
	{
		return 0;       // 空节点 0，返回调用它的位置
	}
	
	if (root->left == NULL && root->right == NULL)
	{
		return 1;  // 左右子树都为空，返回 1， 返回到调用它的位置
	}

	return TreeLeafSize(root->left) + TreeLeafSize(root->right);
	       // 子树的叶子节点          +   右子树的叶子节点
}




// 清空二叉树中的节点
/*
*  注意：在二叉树中的不要把根节点，删除了，不然，无法找到左右子树
*        所以我们需要从最后面，删除，后序遍历类似
*/
void DestroryTree(BTNode* root)
{
	if (NULL == root)
	{
		return;
	}

	DestroryTree(root->left);      // 清空当前节点的左子树
	DestroryTree(root->right);     // 清空当前节点的右子树

	free(root);                    // 释放当前节点的空间

}



// 计算二叉树的节点的个数 ，把每个节点看作是 根左右
/* 方式二，传参数的地址的方式，改变形参影响实参
void TreeSize2(BTNode* root, int* size)
{
	if (NULL == root)  // 递归结束条件
	{
		return;  // 返回，调用你的位置处；
	}
	else    
	{
		(*size)++;  // 解引用，形参改变实参
	}

	TreeSize2(root->left, size);  // 递归,左子树
	TreeSize2(root->right, size); // 递归，右子树


}
*/



/*
void TreeSize2(BTNode* root)
{
	int size = 0;   // 该计算的数值，会在递归中，被不断置为 0，导致无法计数
	if (NULL == root)  // 所以使用传参数的地址的方式
	{
		return;
	}
	else
	{
		size++;
	}

	TreeSize2(root->left);
	TreeSize2(root->right);

}
*/


// 计算二叉树的节点个数
/*方式一，定义全局变量,累计计算
* 存在累计问题
void TreeSize1(BTNode* root)
{

	if (NULL == root)     // 递归结束条件
	{
		return ;
	}
	else
	{
		size1++;    // 节点不为空加加
	}

	TreeSize1(root->left);   // 递归，计算其左子树
	TreeSize1(root->right);  // 递归，计算其右子树

	return ;
}
*/



