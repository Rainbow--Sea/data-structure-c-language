#define  _CRT_SECURE_NO_WARNINGS  1

#include<stdio.h>


// 交换数值
void swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}


// 打印数组
void playPrint(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}

	printf("\n");
}



// 插入排序,升序
void insertSortAsc(int* arr, int n)
{
	for (int i = 0; i < n - 1; i++)  // n -1 防止后面的 end +1 i +1 越界了
	{
		int end = i;
		int tmp = arr[end + 1];   // 后一个比较的数值

		while (end >= 0)    // 防止越界
		{
			if (tmp < arr[end])   // tmp < 其前面的数值,往前走
			{
				arr[end + 1] = arr[end];  // 将比 tmp 大的数值后移动,覆盖掉
				end--;  // 继续向前下标走
			}
			else  // 前面的数值比 tmp 还要小 ,跳出循环 
			{
				break;
			}
		}
		// 跳出循环插入到小于 tmp的数值的后面
		arr[end + 1] = tmp;
		
	}

}



// 插入排序,降序
void insertSortDesc(int* arr, int n)
{
	for (int i = 0; i < n - 1; i++)  // n-1 防止后面的 end + 1 / i+1 越界了
	{
		int end = i;
		int tmp = arr[end + 1];

		while (end >= 0)     // 防止越界
		{
			if (tmp > arr[end])  // tmp 比前面的数值大,前移动
			{
				arr[end + 1] = arr[end];  // 将比 tmp 小的数值后移动,覆盖
				end--;   // end 下标继续向前走
			}
			else     // 前面的数值 比 tmp 大,跳出循环
			{
				break;
			}
		}

		// 将 tmp 插入到大于 tmp 的数值的后面
		arr[end + 1] = tmp;
	}
}



// 三数取中
/* 
*  把所有的三种情况 取中间值都取到来 mid, left, right
*/
int getMidIndex(int* arr, int left, int right)
{
	int mid = (left + right) >> 1; /* 也就是 (left + right) / 2*/

	if (arr[left] < arr[mid])
	{
		if (arr[mid] < arr[right])
		{ // arr[left] < arr[mid] < arr[right] mid 为数值中间的
			return mid;
		}
		else if (arr[left] > arr[right])
		{  // arr[right] < arr[left] < arr[mid]  left 为中间数值
			return left;
		}
		else    // arr[left] < arr[right]
		{  // arr[left] < arr[right] < arr[mid]  right 为中间数值
			return right;
		}
	}
	else   // arr[left] > arr[mid]
	{
		if (arr[mid] > arr[right])
		{  //  arr[right] < arr[mid] < arr[left]  mid 为中间数值
			return mid;
		} 
		else if (arr[left] < arr[right])
		{  //arr[mid] < arr[left] < arr[right]  left 为中间数值
			return left;
		}
		else  // arr[left] > arr[right]
		{     // arr[mid] < arr[right] < arr[left] 
			return right;
		}
	}

}

// 快速排序, 挖坑法,升序
void quickSortAsc(int* arr, int left, int right)
{

	if (left >= right)    // 递归结束条件, 当左右区间只有一个数值时,表明已经是达到有序了,
	{                     // 当left > right 已经是越界了,不可继续了。
		return;
	}

	int begin = left;        // 前下标
	int end = right;         // 后下标
	int pivot = begin;       // 默认坑位,是数组中的第一个元素
	int key = arr[begin];    // 关键值

	while (begin < end)      // 相遇停止循环
	{     // 先总右边开始,右边找中的小于 key 的数值的下标,再通过下标将数值填入左边的坑位中
		while (begin < end && arr[end] >= key)      /* begin < end 防止在该循环中 begin 与 end 已经相遇,并走过了*/
		{                   /* arr[end] >= 需要 = 如果没有 = 的话, 当 arr[end] = key 时, end -- 不会执行的*/
			end--;          /* 就是一直停留在 相等的下标位置,begin 与 end 就无法相遇,导致死循环的发生 */
		}

		// 跳出循环,找到了,右边小于 key 的数值下标
		arr[pivot] = arr[end];  // 将该小于 key 的数值,入左边的坑位            
		pivot = end;            // 自己成为新的坑位.

		// 左边找大于 key 的数值的下标,再通过下标将数值填入到右边的坑位中
		while (begin < end && arr[begin] <= key)      /* begign < end 防止在该循环中 begin 与 end 已经相遇的,并走过了*/
		{                    /* arr[begin] <= 需要 = ,如果没有 = 的话, 当 arr[begin] = key 时,begin ++ 时不会执行的*/
			begin++;         /* 就会一直停留在 相等的下标位置处,就无法与 end 相遇,导致死循环*/
		}

		// 跳出循环,找到了,左边大于 key 的数值下标
		arr[pivot] = arr[begin]; // 将该大于 key 的数值,入右边的坑位
		pivot = begin;           // 自己成为新坑位
	}

	// 当 begin 与 end 相遇的时候,跳出循环, 把 key 入到坑位中
	pivot = begin;   // begin 与 end 相遇点作为坑位,供 key 填入, end 和 begin 是相等的
	arr[pivot] = key; // 将 key 填入到 坑位中,这样 key 也就排序好的,不要动了

	/* 将左区间和右区间有序，我们就整体有序了，使用同样的方式将一个一个的 key 有序*/
	/* [left, pivot-1] pivot [pivot+1, right] */
	/* pivot 已经有序了,就不要再动了 */

	quickSortAsc(arr, left, pivot - 1);     // 递归左区间有序
	quickSortAsc(arr, pivot + 1, right);    // 右区间,递归有序


}
/* 这种方式的快速排序，没有优化，其最坏的情况是有序的时候，
当 1 2 3 4 5 6 为有序排序
  默认取 数组首元素为 key 关键值，则会导致
   左边找大于 key = 1 的数值， 右边找小于 key = 1 的数值 end 走走到 1 的位置 时间复杂度为 N -1 后,与 begin 相遇
   次小的数值 key = 2 的数值，左边找大于 key = 2 的数值, end 走到 2 的位置 时间复杂度为 N -2 后,与 begin 相遇 
   以此类推 时间复杂度为 N*N ;
   我们可以通过三数取中的方式：不要出现这种极端的 key = 最大值或最小值之类的情况，导致时间复杂度为 N*N的情况。
*/


// 优化关键值 key 为 三数取中的数值，避免 有序 key = 最小值或最大值 的最坏情况
void quickSort2(int* arr, int left, int right)
{
	if (left >= right)      // 当 左右区间的数值 只有一个数据,或者left >= right 不存在时,说明这部分已经有序了
	{
		return;
	}

	int index = getMidIndex(arr, left, right);   // 三数取中 ,数值下标
	swap(&arr[left], &arr[index]);               // 交换数组首元素的,与三数取中的数值

	int begin = left;       // begin 下标为三数取中的数值,因为交换了
	int end = right;
	int pivot = begin;      // 三数取中的数值的下标,作为坑位
	int key = arr[pivot];   // 三数取中的数值

	while (begin < end)
	{
		// 右边 end 先走, 找比 key 小的数值
		while (begin < end && arr[end] >= key)
		{
			end--;
		}

		// 跳出循环,找到了,小于 key 的数值下标
		arr[pivot] = arr[end];   // 将小于 key 的数值入填坑位,
		pivot = end;             // 自己成为新的坑

		// 左边 找 key 大 的数值
		while (begin < end && arr[begin] <= key)
		{
			begin++;
		}

		// 跳出循环,找到了,大于 key 的数值下标
		arr[pivot] = arr[begin];    // 将大于 key 的数值填入坑位
		pivot = begin;              // 自己成为新的坑
	}

	pivot = begin;                  // 相遇了,相遇点作为,新的坑位
	arr[pivot] = key;               // 将 key 数值入坑位,key 就排序好了

	// 将左右区间排序好,整体就有序了,同样使用相同的方法,将 左右区间中的数值一个一个的key 排序到指定位置
	// [left, pivot-1] pivot [pivot +1, right]
	quickSort2(arr, left, pivot - 1);  // 左区间,递归排序好
	quickSort2(arr, pivot + 1, right); // 右区间,递归排序好

}



// 区间优化,消除递归深度
void quickSort3(int* arr, int left, int right)
{
	if (left >= right)   // 当只有一个数值,或者 left > right 不存在时,表示该排序部门已经有序了
	{
		return;
	}

	int index = getMidIndex(arr, left, right); // 三数取中的数值的下标位置
	swap(&arr[index], &arr[left]);  // 首元素和三数取中的数值交换数值

	int begin = left;
	int end = right;
	int pivot = begin;     // 坑位下标,就是三数取中的数值下标,因为交换了
	int key = arr[pivot];  // 三数取中的数值,因为交换了

	while (begin < end)
	{
		// 先从,右边开始找 小于 key 的数值的下标位置
		while (begin < end && arr[end] >= key) // begin < end 防止在该循环中,已经有 begin 与 end 相遇，走过了
		{            // >= 需要等于, 如果没有 等于 的话,当 arr[end] = key 时,是不会执行 end -- 的
			         // 就会导致 end 一直停留在 相等的位置, 导致 begin 与 end 无法相遇,死循环。
			end--;
		}

		// 跳出循环找到了,小于 key 的数值下标
		arr[pivot] = arr[end];  // 将小于key 的数值,填入坑位
		pivot = end;    // 自己成为新的坑位

		// 左边找大于 key 的数值下标位置
		while (begin < end && arr[begin] <= key)
		{
			begin++;
		}

		// 跳出循环找到了,大于 key 的数值下标
		arr[pivot] = arr[begin];  // 填入,右边的坑位
		pivot = begin;     // 自己成为新的坑位

	}

	// 相遇跳出循环, 

	pivot = begin;  // 相遇点,作为新的坑位
	arr[pivot] = key; // key 入坑位,将 key 排序好

	// 小区间,清除递归,换用插入排序
	// [left , pivot-1] pivot [pivot+1 ,right]
	if (pivot - 1 - left > 10)  // 当左区间中的数量大于 10 时，继续使用快速排序,递归左区间
	{
		quickSort3(arr, left, pivot - 1);   // 左区间,递归排序
	}
	else  // 当左区间中的数量小于 10 时,不再使用快速排序递归了,而是使用插入排序,让剩余的数有序
	{
		insertSortAsc(arr+left, pivot - 1 - left + 1);  // 插入排序
		// 左区间：插入排序时,需要从高 arr+left 数组位置开始,到pivot-1-left+1 ，+1 是因为插入排序是数组的长度不是最后一个下标位置
		// [left, pivot-1] pivot [pivot+1,right]
	}
	            // 注意是整体(pivot+1) 不要忘了括号,不然意义就变成是 +1了
	if (right - (pivot + 1) > 10)   // 右区间数量大于 10 ,继续使用递归快速排序,递归右区间
	{
		quickSort3(arr, pivot + 1, right);  // 递归右区间,有序
	}
	else
	{
		// 右区间,插入排序剩下的数
		insertSortAsc(arr + pivot+1, right - (pivot + 1) + 1);   // 注意:是减 (prvot+1)不要把括号忘了,不然会越界的
		// 右区间,插入排序,需要从 arr+pivot +1 数组位置开始,到 right -(pivot+1)+1 ,
		// 因为插入排序,是数组的长度,不是数组最后一个下标位置, +1
		// [left, pivot -1] pivot [pivot+1, right]
	}

	
}



// 快速排序,降序
void quicSortDesc(int* arr, int left, int right)
{
	if (left >= right)   // 当 左右区间只有一个数值时,或者 left > right 序列不存在时,该部门有序了,返回
	{
		return;
	}

	int index = getMidIndex(arr, left, right);   // 三数取中的数值下标,防止关键值key 为最大值或最小值,以及有序
	swap(&arr[left], &arr[right]);  // 首元素,与三数取中,交换数值,防止影响到后面的操作

	int begin = left;
	int end = right;
	int pivot = begin;
	int key = arr[pivot];  // key 坑位上的数值,保留,并用于比较

	while (begin < end)
	{
		// 先从右边开始,找大于 key 的数值下标
		while (begin < end && arr[end] <= key)  // begin < end 防止在该循环中 begin 与 end 已经相遇过了
		{     // >= key ,需要等于 如果没有等于的话,当 arr[end] = key 时,end -- 是不会执行的,
			end--;               // 就会导致,end 一直停留在 等于的下标位置,这样 begin 与 end 是不会相遇的死循环
		}

		// 跳出循环找到了, 小于 key 的数值下标位置
		arr[pivot] = arr[end];   // 将小于 key 的数值,填入到左边坑位中
		pivot = end;      // 自己变成新的坑位

		 // 左边找小于 key 的数值下标位置
		while (begin < end && arr[begin] >= key)
		{
			begin++;
		}

		// 跳出循环,找到了小于 key 的数值下标
		arr[pivot] = arr[begin];   // 将小于 key 的数值填入到右边坑位中
		pivot = begin;   // 自己成为新的坑位
	}

	// 相遇跳出循环
	pivot = begin; // 相遇点作为坑位, begin 与 end 是相等的
	arr[pivot] = key;  // 将 key 填入坑位, key 排序好了

	// 小区间,使用插入排序,消除递归
	// [ left, pivot -1] pivot [pivot +1, right]  pivot 已经排序好了,不要动

	if ((pivot - 1) - left > 10)   // 如果左区间,的数量大于 10 继续使用快速排序,递归处理
	{
		quicSortDesc(arr, left, pivot - 1);
	}
	else   // 如果左区间,的数量小于 10 使用插入排序,处理剩下的数值
	{
		insertSortDesc(arr + left, pivot - 1 - left + 1);  // 插入排序
		// 注意插入排序的参数是 (数组,数组的长度)
		// 需要arr +left 开始, 到 pivot -1 -left+1 的数组长度 
		// pivot -1 -left 是左区间 [left, pivot-1] +1 是数组的长度,不是 数组最后一个下标位置
		// [0~9] 共有 10 个数
	}
	            // 注意是整体(pivot +1),不要把括号忘了,意义就不一样了+1
	if (right - (pivot + 1) > 10)  // 右区间数量大于 10 继续使用快速排序,递归排序
	{
		quicSortDesc(arr, pivot + 1, right);
	}
	else   // 右区间数量小于 10 ,使用插入排序将剩下的数值排序好
	{                                       // 注意是:(pivot +1) 整体需要加上括号
		insertSortDesc(arr + (pivot + 1), right - (pivot + 1) + 1); // 右区间
		// [left, pivot-1] pivot [ pivot+1, right]
		// 插入排序的参数(数组地址,数组的大小)
		// 右区间从 arr+pivot +1 的位置开始, right - (pivot+1)区间数量 +1 数组的大小;
	}

}



// 快速排序,升序,左右指针法
void quickSortLeftRightAsc(int* arr, int left, int right)
{
	if (left >= right)    // 当左右区间中的数量只有一个数值时 或者 left > right 不存在时,表示该部门已经有序了
	{
		return;
	}

	int index = getMidIndex(arr, left, right);  // 三数取中的数值的下标位置
	swap(&arr[index], &arr[left]);       // 交换数组首元素 与三数取中的数值,防止 key 关键值划分为 最大值,最小值,导致N*N

	int begin = left;  // 左边下标
	int end = right;   // 右边下标
	int keyi = begin;   // 关键值划分,三数取中的数值下标,因为交换了

	while (begin < end)
	{
		// 右边，找小于 key 的数值的下标位置 
		while (begin < end && arr[end] >= arr[keyi]) // begin < end 防止在该循环中 begin 与 end 已经相遇
		{     // >= key 需要 = 如果没有 等于的话,当 arr[end] = key 时,就会导致 end -- 不会执行到,
			end--;     // 停留在与 相等的位置, begin 与 end 就不会相遇了,导致死循环
		}

		// 左边,找大于 key 的数值的下标位置
		while (begin < end && arr[begin] <= arr[keyi])
		{
			begin++;
		}

		swap(&arr[end], &arr[begin]);
	}

	swap(&arr[begin], &arr[keyi]);
	int meet = begin;  // meet 作为相遇点,排序好的数值位置,不要动
	// 小区间,消除递归,使用插入排序
	// [left, meet -1] meet [meet+1, right] meet 下标位置的数值排序好了,不要动


	if (((meet - 1) - left) > 10)
	{  // 左区间 中的数量大于 10 继续使用快速排序,递归排序
		quickSortLeftRightAsc(arr, left, meet - 1);  // 左区间,递归快速排序
	}
	else
	{
		insertSortAsc(arr + left, (meet - 1 - left) + 1);
		// 插入排序参数( 数组,数组的大小)
		// arr+left 开始, 到 meet-1 -left 表示数组区间+1 数组的大小
		// [0~9] 数组大小是 9 +1 = 10
	}
	             // 注意是整体(meet+1),不要把括号忘了,不然意义就变成了 +1了
	if ((right - (meet + 1)) > 10)   // 右区间,的数量大于 10 继续使用 快速排序,
	{
		quickSortLeftRightAsc(arr, meet + 1, right);   // 右区间,递归快速排序
	}
	else
	{                               // 注意是:（meet +1) 需要加括号,不然会越界,
		insertSortAsc(arr + meet+1, right - (meet + 1) + 1);
		// [left, meet-1] meet [meet+1, right] meet 是相遇点,排序好的
		// 插入排序的参数(数组,数组的大小)
		//右区间 arr+meet+1 数组地址开始, right - meet+1 右区间的范围，+1 数组的大小
		// [ 0~9] 数组的大小是 9+1 = 10;
	}
	

}



// 快速排序,降序 左右指针法,
void quickSortLeftRightDesc(int* arr, int left, int right)
{
	if (left >= right)  // 当左右区间中只有一个数值或者 不存在 left > right 序列时,表示该部分已经有序了
	{
		return;
	}

	int index = getMidIndex(arr, left, right);  // 三数取中,防止key关键值为最大值或最小值,时间复杂度N*N
	swap(&arr[left], &arr[index]);   // 因为左右指针,需要从最开头和最后面开始，
	int begin = left;   // 左指针
	int end = right;    // 右指针
	int keyi = begin;   // 关键值所在的下标位置,

	while (begin < end)
	{
		// 左边找大于 keyi 下标数值的下标
		while (begin < end && arr[end] <= arr[keyi]) // begin < end 防止在该循环中就已经有 begin与end相遇了,而没有退出
		{                 // <=  需要等于,如果没有等于的话,当 arr[end] = arr[keyi] 是不会执行 end-- 的
			end--;        // 就会导致 begin 与 end 无法相遇,死循环
		}

		// 右边找小于 keyi 下标的数值的下标
		while (begin < end && arr[begin] >= arr[keyi])
		{
			begin++;
		}

		// 跳出循环找到了,交换
		swap(&arr[begin], &arr[end]);
	}

	// 相遇了,将keyi 下标下的数值交换
	swap(&arr[begin], &arr[keyi]);
	int meet = begin;   // 相遇点

	// 消除区间, 清除递归,使用插入排序
	// [left meet-1] meet [ meet+1, right]
	if (meet - 1 - left > 10)  // 左区间数量 大于 10 ,继续使用递归快速排序
	{
		quickSortLeftRightDesc(arr, left, meet - 1); // 左区间,递归快速排序
	}
	else   // 左区间数量 小于 10 ,则剩下的使用插入排序,
	{
		insertSortDesc(arr + left, meet - 1 - left + 1);  // 左区间,插入排序
		// 插入排序参数(数组地址,数组的大小)
		// 左区间是在 [left, meet-1]
		// 所以是 arr+left, meet-1-left 区间的数量 +1 数组的大小 [0~9] 9-0+1 = 10
	}
	             // 注意是整体(meet+1) 不要把括号忘了,不然意义就变成了+1了
	if (right - (meet + 1) > 10) // 右区间数量大于 10 的话,继续使用递归快速排序
	{
		quickSortLeftRightDesc(arr, meet + 1, right);  // 右区间,递归快速排序
	}
	else
	{                                     // 注意需要加(meet+1)括号,不然会越界
		insertSortDesc(arr + meet + 1, right - (meet + 1) + 1); // 右区间,插入排序
		// 插入排序参数（数组地址,数组的大小)
		// 右区间是在[meet+1,right] 
		// 所以 arr+meet+1, right -（meet+1) 区间数量 +1 数组的大小，[0~9] 9-0+1= 10
	}

}



// 前后指针法,升序
void quickSortFrontBackAsc(int* arr, int left, int right)
{
	if (left >= right)  // 当左右区间数量为 1(相等)的时候 或者 left > right 不存在的序列,该部分已经有序了
	{
		return;
	}

	int index = getMidIndex(arr, left, right);  // 三数取中,防止keyi 为最大值或最小值,时间复杂度为 N*N
	swap(&arr[index], &arr[left]);   // 三数取中的数值交换 首元素数值交换,因为前后从 0 开始

	int front = left+1;   // 前指针比后指针多+1个下标位置
	int back = left;      // 后指针
	int keyi = left;      // keyi 关键值下标位置

	while (front <= right)   // front <= 最后一个数组的下标位置
	{
		if (arr[front] < arr[keyi])    // 小于keyi 关键值与 back++ 交换
		{
			back++;
			swap(&arr[back], &arr[front]);
		}

		front++;  // 前指针继续，走
	}
	// 到达最后,与 keyi 交换数值
	swap(&arr[keyi], &arr[back]);

	int good = back;    // 排序好的数值的下标位置
	// 小区间,消除递归,插入排序
	//[left, good-1] good [good+1, right] goot 排序好的不要动

	if (good - 1 - left > 10)   // 左区间数量大于 10 ,继续使用,递归快速排序
	{
		quickSortFrontBackAsc(arr, left, good - 1);  // 左区间递归,快速排序
	}
	else   // 左区间数量小于 10,使用插入排序
	{
		insertSortAsc(arr + left, good - 1 - left + 1); // 左区间,插入排序
		// 插入排序的参数(数组地址,数组的大小)
		// 左区间[left, good-1] 
		// 所以 arr+left, good-1 - left 左区间 +1 左区间的大小 [0~9] 9-0+1 = 10
	}

	// 注意是整体 (good+1) 不要把括号省略了,意义就是 +1了
	if (right - (good + 1) > 10) // 右区间数量大于 10,继续使用递归快速排序
	{
		quickSortFrontBackAsc(arr, good + 1, right);
	}
	else // 右区间数量小于 10 ,使用插入排序
	{                             // 注意是整体减(good+1) 需要加括号,不然会越界访问
		insertSortAsc(arr + good + 1, right - (good + 1) + 1);
		// 插入排序的参数(数组地址,数组的大小)
		// 右区间[good+1, right] 
		// 所以 arr+good+1, right-(good+1) 右区间的数量 +1 大小 [0~9] 9-0+1= 10
	}
}



// 前后指针,降序
void quickSortFrontBankDesc(int* arr, int left, int right)
{
	if (left >= right)  // 当左右区间中,只有一个数值的话,或者 left > right 不存在的情况,表示该部分已经有序了
	{
		return;
	}

	int index = getMidIndex(arr, left, right);  // 三数取中,防止 keyi 是最大值/最小值,O(n*n)
	swap(&arr[left], &arr[index]);   // 三数取中的数值与数组中首元素交换,前后是从下标 0 开始的
	int front = left + 1;
	int bank = left;
	int keyi = left;   // 关键数值的下标位置

	while (front <= right)   // 防止越界
	{      // 降序找大于交换
		if (arr[front] > arr[keyi])
		{
			bank++;
			swap(&arr[bank], &arr[front]);
		}

		front++;
	}

	swap(&arr[bank], &arr[keyi]);  // keyi 排序好了

	int good = bank;

	// 小区间,消除递归,插入排序
	// [left, good - 1] good[goo + 1, right];   good 有序了不要动

	if (good - 1 - left > 10)  // 左区间中数量大于 10 ,继续使用快速排序,递归
	{
		quickSortFrontBankDesc(arr, left, good - 1); // 左区间递归
	}
	else  // 左区间中数量小于 10 ,使用插入排序,剩下的数据
	{
		insertSortDesc(arr + left, good - 1 - left + 1);  // 左区间,插入排序
		// 插入排序的参数(数组的地址,数组的大小)
		// 左区间[left,good-1]
		// 所以arr+left,good-1 -left 是左区间的数量 +1 是数组的大小 [0~9] 9-0+1= 10
	}

	// （good+1) 整体不要把括号去了,不然就会变成+1 了
	if (right - (good + 1) > 10)  // 右区间数量大于 10 ,继续使用递归快速排序
	{
		quickSortFrontBankDesc(arr, good + 1, right); // 右区间,递归快速排序
	}
	else  // 右区间小于 10 ,使用插入排序,剩下的数据
	{                                 // 是减整体 (good+1),不然会越界报错
		insertSortDesc(arr + good + 1, right - (good + 1)+1); // 右区间
		// 插入排序的参数(数组的地址,数组的大小)
		// 右区间[good+1, right]
		// 所有good+1, right - (good+1) 右区间的范围,+1 大小,[0~9] 9-0+1= 10;
	}
}



/*
递归的缺陷: 栈帧深度太深,栈空间不够用,可能会导致溢出
递归改为非递归的方式：
1. 直接改为循环(斐波那契额数列)
2. 借助自己或库中的栈,模拟递归过程。栈帧过程中的,对数据的进栈出栈
*/


void quickSortTest()
{
	int arr[] = { 6,1,2,7,9,3,4,5,10,8 };
	//quickSort2(arr, 0, sizeof(arr) / sizeof(int) - 1);   // 优化三数取中
	//quickSortAsc(arr, 0, sizeof(arr) / sizeof(int) - 1);   // -1 传的为数组的最后一个下标位置。
	//insertSortAsc(arr, sizeof(arr) / sizeof(int));       // 插入排序,升序
	//insertSortDesc(arr, sizeof(arr) / sizeof(int));      // 插入排序,降序
	//quickSort3(arr, 0, sizeof(arr) / sizeof(int) - 1);   // 三数取中,小区间消除递归,

	//quicSortDesc(arr, 0, sizeof(arr) / sizeof(int) - 1);   // 快速排序,降序

	//quickSortLeftRightAsc(arr, 0, sizeof(arr) / sizeof(int) - 1);  // 快速排序,左右指针,升序

	//quickSortLeftRightDesc(arr, 0, sizeof(arr) / sizeof(int) - 1); // 快速排序,左右指针,降序

	// quickSortFrontBackAsc(arr, 0, sizeof(arr) / sizeof(int) - 1); // 快速排序,前后指针,升序

	quickSortFrontBankDesc(arr, 0, sizeof(arr) / sizeof(int) - 1);   // 快速排序,前后指针,降序
	playPrint(arr, sizeof(arr) / sizeof(int));

	/* sizeof(arr)/sizeof(int) 表示的是数组的长度 
	   - 1 表示的是数组最后一个数值的下标位置*/
	/* sizeof(arr) 表示数组的所占空间的大小 单位 字节 */
	/* sizeof(int) 表示数组的元素类型所占空间的大小 单位 字节 */

}


int main()
{
	// 快速排序测试
	quickSortTest();


	return 0;
}