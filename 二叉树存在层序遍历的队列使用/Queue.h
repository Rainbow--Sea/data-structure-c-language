#pragma once
#define  _CRT_SECURE_NO_WARNINGS  1

#include<stdio.h>
#include<assert.h>  // 断言
#include<stdbool.h>  // 布尔值
#include<stdlib.h>   // malloc
struct BinaryTreeNode;

typedef struct BinaryTreeNode* QDataType;    // 定义存放队列中的数据类型

typedef struct QueueNode   // 创建队列结构体
{
	struct QueueNode* next;   // 节点指针，注意这里，不可以是 QNode* next ，因为执行顺序的原因，它还没有创建
	QDataType data;           // 别名上的使用 。存在的数据

}QNode;


typedef struct Queue       // 创建队列，队头，队尾的结构体
{
	QNode* head;          // 队列的首节点
	QNode* tail;          // 队列的尾节点

}Queue;


extern void QueueInit(Queue* pq);         // 初始化队列
extern void QueueDestory(Queue* pq);     // 删除队列
extern void QueuePush(Queue* pq, const QDataType x);     // 队尾，入队
extern void QueuePop(Queue* pq);                       // 队头，出队
extern QDataType QueueFront(const Queue* pq);          // 取队头数据
extern QDataType QueueBack(const Queue* pq);           // 取队尾数据
extern int QueueSize(const Queue* pq);                 // 计算队列中存在多少元素
extern bool QueueEmpty(const Queue* pq);               // 判断队列是否为空
extern void test();                                    // 队列测试



