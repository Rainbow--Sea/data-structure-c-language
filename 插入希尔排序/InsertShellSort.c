#define  _CRT_SECURE_NO_WARNINGS  1


#include<stdio.h>
#include<stdlib.h>
#include<time.h>

// 数组遍历，打印

void printArray(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}

	printf("\n");
}


// 插入排序,升序排序
/*
*  插入排序的时间复杂度为 O(n*n) ,注意不是看循环，而是看最坏的情况下
*  插入排序的最坏的情况是 在这里的升序是，逆序，987654321 ，每次都要移动 （n-1) (n -2) 次 时间复杂度为 O(n*n)
*  插入排序的最好的情况是 在这里的升序是 ，顺序 ，123456789 ，每次都不用移动，循环不进入，直接出循环后面插入数值， 时间复杂度为O(n)
*  对于数据越有序的，插入排序越快，与后面的希尔排序同理
*/
void InsertSortAsc(int* arr, int n)
{
	for (int i = 0; i < n -1 ; i++)    // 注意 是 n-1, 不是 n ,因为数组下标从 0 开始，我们只能取到倒数第二个数值，end+1 会取到最后一个数值
		                               // 为 n 就 end +1 所取是值就是越界了。
	{
		int end = i;      // 数组下标位置
		int tmp = arr[end + 1];  // 数组后一个下标的，插入排序，拷贝保存，防止被移动覆盖了，丢失了，数据

		while (end >= 0)      // 注意 下标为 0 也要比较判断插入
		{
			if (tmp < arr[end])   // tmp 小于 其前面的下标的数据，
			{
				arr[end + 1] = arr[end];   // 前面的数据后移动，覆盖
				end--;                     // 向前继续判断，
			}
			else
			{
				break;                   // tmp 大于 其前面的下标的数据，跳出循环，插入数据
			}
		}
		// 跳出循环时，tmp 大于其下标位置的数值，插入其下标位置的后面
		arr[end + 1] = tmp;             // 在其小于 tmp 的下标位置插入该数据
	}
}




// 插入排序，降序
void InsertSortDesc(int* arr, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int tmp = arr[end + 1];

		while (end >= 0)         // 下标不可小于 0 防止越界
		{
			if (tmp > arr[end])  // 升序，tmp > 其前面的数值前移动
			{
				arr[end + 1] = arr[end];
				end--;           // 前移动下标，一个一个比较判断
			}
			else
			{
				break;         // tmp 数值比前面的数值小 或等于时，跳出循环插入到，其数值的后面
			}
		}
		// 跳出循环时，其下标位置的数值，大于 tmp ,插入到其下标的后面 
		arr[end + 1] = tmp;
	}
}





// 希尔排序，升序
/* 1. 预排序 在升序里，就是把小的数值的数据放到前面，不大不小的数值放到中间，大的数值放到后面
*  2. 最后，插入排序成有序的
*/
/* 所谓的希尔排序，其实就是插入排序的一种优化，
通过分组，预排序，形成一定的有序，每一步的 gap 分组都在减少移动的次数，提高效率
为最后一次的 gap == 1的插入排序，减少了大量的移动数据量，大大提高的排序的效率
希尔排序的时间复杂度为 gap = n / 2 O(nlog2^n) 以2为底n的对数
                     gap = n / 3 +1 O(nlog3^n) 以3为底n的对数
*/
void shellSortAsc(int* arr, int n)
{
	int gap = n ;  // 预排序，中的分隔组大小 ,注意 gap 最后必须等于 1 
	/*
	*  gap 越大，大的数值就越快放置到后面，小的数值可以越快到前面
	*  gap 越大，数据越无序
	*  gap 越小，数据越有序
	*  gap == 1 直接变成插入排序
	*/
	
	while (gap > 1)    // 大于 1 防止无用 gap = 0 无效的分组
	{
		gap = gap / 3 + 1;   // gap / 3 +1 无论 gap 变成何数值，最后都可以变成 1， 2 /3 +1 = 1
		// 或者是 gap /2 同样 gap 变成何数值，最后都可以变成 1，  3 / 2 = 1 0/2 = 0，注意了 gap 是大于 1 的

		// 间隔分组为 gap 多组同时，排序预排序
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;   
			int tmp = arr[end + gap];    // 同插入排序一样，保存数值防止覆盖，不过这里是以 gap 分组下一个下标位置

			while (end >= 0)              // 同样下标不为 0 防止越界
			{
				if (tmp < arr[end])          // tmp 比前面下标的数值小，向前移动
				{
					arr[end + gap] = arr[end];   // 移动数据覆盖，
					end = end - gap;             // 因为分组了，所以向前移动就是以 gap 分隔移动了。
				}
				else   // tmp >= 前面的数值，停止移动，在其下标位置插入该数值
				{
					break;
				}

			}

			// 其下标位置的数值 <= tmp ,tmp插入到该下标后面位置处
			arr[end + gap] = tmp;  
		}

 
	}

}




/*
* 希尔排序的，降序
* 1.预排序，gap 减少移动的数据量
* 2. gap == 1, 直接插入排序
*/
void shellSortDesc(int* arr, int n)
{
	int gap = n;

	while (gap > 1)  // gap 不等于 0 ，无用的分组间隔
	{
		gap = gap / 2;  // gap 分组间隔，最后一次要为 1，插入排序

		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = arr[end + gap];   // gap 分组间隔，为下一个下标所在位置

			while (end >= 0)
			{
				if (tmp > arr[end])     // tmp 大于 前面的以 gap 分隔的下标的数值，
				{
					arr[end + gap] = arr[end];  // 小于的数值后移动 注意是以 gap 间隔移动
					end = end - gap;            // 向前面的以 gap 为间隔的数据移动
				}
				else    // tmp <= 前面以 gap 分隔的下标的数值，跳出循环，插入数据
				{
					break;
				}
			}

			//  tmp <= 前面以 gap 分隔的下标的数值，将其tmp 数值插入到该下标以 gap 分隔的数据
			arr[end + gap] = tmp;
		}
	}

}


void Test()
{

	int arr[] = { 5,7,8,2,1,4,8,9,0,3 };
	// 希尔排序，降序
	shellSortDesc(arr, sizeof(arr) / sizeof(int));
	printArray(arr, sizeof(arr) / sizeof(int));

	//  希尔排序，升序
	shellSortAsc(arr, sizeof(arr) / sizeof(int));  // sizeof(arr)/sizeof(int) 计算数组的个数的大小
	printArray(arr, sizeof(arr) / sizeof(int));

	// 插入排序，升序
	InsertSortAsc(arr, sizeof(arr) / sizeof(int));   // sizeof(arr)/ sizeof(int) 计算数组的个数，
	// 插入排序，降序
	InsertSortDesc(arr, sizeof(arr) / sizeof(int)); 
	// 打印数组
	printArray(arr, sizeof(arr) / sizeof(int));   // sizeof(arr):表示数组的大小 单位字节，sizeof(int) :表示数组元素的类型的大小

	
}



// 测试排序的性能
void TestoP()
{
	srand(time(0));   //  设置随机值，种子

	const int N = 100000; // 10 万
	
	int* arr1 = (int*)malloc(sizeof(int) * N);     // 动态开辟 10 万个堆空间
	int* arr2 = (int*)malloc(sizeof(int) * N);     // 动态开辟 10 万个堆空间

	// 动态堆区上空间开辟失败，
	if (NULL == arr1 || NULL == arr2)
	{
		perror("mallo error");    // 提示错误
		exit(-1);      // 退出程序
	}

	for (int i = 0; i < N; i++)
	{
		arr1[i] = rand();   // 产生随机值，并赋值到数组中
		arr2[i] = arr1[i];  // 数组赋值，让数组中是数值一样，控制变量，排序
	}

	int begin1 = clock();    // clock() 返回进程处理的时间，单位毫秒
	InsertSortAsc(arr1, N);  // 插入排序，升序
	int end1 = clock();      // clock() 返回进程处理的时间，单位毫秒

	int begin2 = clock();    // clock() 返回进程处理的时间，单位毫秒
	shellSortAsc(arr2, N);   // 希尔排序，升序
	int end2 = clock();      // clock() 返回进程处理的时间，单位毫秒

	/* 
	* 通过排序后的时间 end - begin 排序前的时间 差就是排序过程中的时间
	*/
	printf("插入排序的的效率：%d\n", end1 - begin1);
	printf("希尔排序的的效率：%d\n", end2 - begin2);


	// 释放动态堆区开辟的空间
	free(arr1);
	arr1 = NULL;
	free(arr2);
	arr2 = NULL;

}



// 1000 条数据排序结果验证
void testKilo()
{
	// srand(time(0));   // 设置时间种子

	const int N = 1000;

	int* a = (int*)malloc(sizeof(int) * N);
	int* b = (int*)malloc(sizeof(int) * N);  // 动态堆区开辟空间


	// 动态堆区空间开辟是否成功，判断
	if (NULL == a || NULL == b)
	{
		perror("malloc error");
		exit(-1);
	}

	for (int i = 0; i < N; i++)
	{
		a[i] = rand();   //  rand() 产生随机值
		b[i] = rand();   // rand() 产生随机值
	}


	InsertSortAsc(a, N);    // 插入升序排序
	printArray(a, N);       // 打印数组

	printf("*********************************************************************\n");

	shellSortAsc(b, N);     // 希尔升序排序
	printArray(b, N);       // 打印数组
}

int main()
{
	// Test();    排序测试
	// TestoP();  排序性能测试

	testKilo(); // 排序效果测试



	return 0;
}

