#pragma once
#include<stdio.h>
#include<string.h>
#include<assert.h>   // 断言
#include<stdlib.h>


/*
* 注意：一点就是我们的头节点，不要从存放所谓的数值(数据个数)
* 因为当我们定义的结构体的数据存放的类型是 (char,double)的时候，
* 结构体(char，double)类型的解引用，的后果：char的最大存放的数量是 255
* 而double浮点数可以作为计数吗？
* 
*/
typedef int LTDataType;

typedef struct ListNode
{
	LTDataType data;               // 数据
	struct ListNode* next;  // 双链表的后继
	struct ListNode* prev;  // 双链表的前驱
}ListNode;

extern void Test1();          // 测试的声明，声明使用 extern 代码好习惯
extern void Test2();          // 测试的声明
extern ListNode* ListInit();  // 创建头节点，就是所谓的哨兵
extern ListNode* BuyListNode(const LTDataType x);                 // 为双向循环链表，创建单节点，开辟空间
extern void ListPushBack( ListNode* phead, const LTDataType x);   // 尾插法
extern void ListPrint(const ListNode* phead);                     // 打印双向循环链表 
extern void ListPushFront(const ListNode* phead, const LTDataType x);  // 头插法
extern void ListPopBack(const ListNode* phead);               // 尾删法
extern void ListPopFront(const ListNode* phead);              // 头删法
extern void ListInsert(const ListNode* pos, const LTDataType x, const LTDataType num); // 在值为 x 的节点前插入数据
extern void ListErase(const ListNode* pos, const LTDataType x);                        // 删除值为 x 的节点
extern void ListUpdate(const ListNode* pos, const LTDataType x, const LTDataType num); // 将第一个数值为 x 的节点修改
extern void ListDestory(const ListNode* phead);               // 清空链表


