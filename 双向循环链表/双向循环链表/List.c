#define  _CRT_SECURE_NO_WARNINGS  1

#include"List.h"


// 创建头节点，就是所谓的哨兵位
ListNode* ListInit() 
{
	ListNode* phead = BuyListNode(0);

	phead->next = phead;      // 头节点的前驱
	phead->next = phead;      // 头节点的后继
	phead->data = 0;          // 头节点数值位置为空，啥也不放
	return phead;

	/*
	* 头节点，哨兵位，双向循环的链表
	* 根据特征，头节点，头节点的“后继”和“前驱”都是指向它自己(本身)的
	*/
}



// 为双向循环链表，创建单节点，开辟空间
ListNode* BuyListNode(const LTDataType x)   
{

	ListNode* newNode = (ListNode*)malloc(sizeof(ListNode));
	newNode->next = NULL;
	newNode->prev = NULL;
	newNode->data = x;
	return newNode;

}



// 尾插法，找到尾节点，注意循环插入
void ListPushBack(ListNode* phead,const LTDataType x)
{
	ListNode* newNode = BuyListNode(x);  // 生成节点
	ListNode* tail = phead;              //  拷贝头节点，代替头节点的，移动，这样就不必在意顺序了
	
	while (tail->next != phead)          // 移动，找到尾节点
	{
		tail = tail->next;               // 移动节点
	}

	tail->next = newNode;
	newNode->prev = tail;
	newNode->next = phead;
	phead->prev = newNode;

}



// 打印双向循环链表
void ListPrint(const ListNode* phead)
{
	assert(phead);                  // 断言该链表不为“空”为空不用找了
	ListNode* cur = phead->next;    // 拷贝首节点，代替首节点的移动,注意 头节点不用打印，从首节点开始
	
	while (cur != phead)            // 判断遍历完
	{
	      
		printf("%d ", cur->data);
		cur = cur->next;            // 移动节点
	}
	printf("\n");

}



// 头插法
void ListPushFront(const ListNode* phead, const LTDataType x)
{
	assert(phead);                     // 头节点，不可以为 “空”
	ListNode* cur = phead;             // 头节点的拷贝
	ListNode* phNext = phead->next;    // 首节点的拷贝
	ListNode* newNode = BuyListNode(x); // 所需插入的节点的创建

	cur->next = newNode;
	newNode->prev = cur;
	newNode->next = phNext;
	phNext->prev = newNode;

}



// 尾删法
void ListPopBack(const ListNode* phead)
{
	assert(phead);  // 断言头节点不可以为空
	assert(phead->next != phead); // 当只有头节点时不能删除 
	
	ListNode* tail = phead->prev; // 头节点的前驱就是尾节点
	ListNode* prev = tail->prev;  // 尾节点的前驱节点；
	ListNode* copyPhead = phead;  // 头节点的拷贝
	
	prev->next = phead;
	copyPhead->prev = prev;
	free(tail);                   // 释放空间内存
	tail = NULL;                  // 手动置为 NULL
}



// 头删法
void ListPopFront(const ListNode* phead)
{
	assert(phead);                  // 头节点不能为空
	assert(phead->next != phead);   // 当只剩下头节点的时候，不能删除
	
	ListNode* copyPhead = phead;     // 头节点的拷贝
	ListNode* tail = phead->next;    // 首节点的拷贝
	tail->prev = phead;
	copyPhead->next = tail->next;
	free(tail);                      // 释放节点，空间内存
	tail = NULL;                     // 手动置为NULL

}



// 查找值为 x 的节点的地址
static ListNode* listFind(const ListNode* phead, const LTDataType x)
{ 
	assert(phead);                     // 断言该链表的头节不为 “空”
	ListNode* cur = phead->next;       // 首节点开始
	
	while (cur != phead)    
	{
		if (x == cur->data)
		{
			return cur;               // 找到了，返回该地址
		}
		cur = cur->next;              // 移动节点
	}

	return NULL; // 没有找到，返回 NULL;
	
}



// 在值为 x 的节点前插入数据
void ListInsert(const ListNode* pos, const LTDataType x,const LTDataType num)
{

	if (listFind(pos, x) == NULL)
	{
		printf("不用找了 %d，该数值不存在\n",x);
		return;
	}
	
	// 该节点的数值，存在
	ListNode* tail = listFind(pos,x);      // 该节点的地址
	ListNode* prev = tail->prev;           // 该节点位置的前驱节点的地址
	ListNode* newNode = BuyListNode(num);  // 创建节点
	
	tail->prev = newNode;
	newNode->next = tail;
	prev->next = newNode;
	newNode->prev = prev;

}



// 删除值为 x 的节点
void ListErase(const ListNode* pos,const LTDataType x)
{
	if (listFind(pos, x) == NULL)
	{
		printf("不用找了 %d，该数值不存在\n", x);
		return;
	}
	
	ListNode* tail = listFind(pos, x);  // 该节点的地址
	ListNode* prevTail = tail->prev;    // 该节点的前驱的地址
	ListNode* nextTail = tail->next;    // 该节点的后继的地址
	
	prevTail->next = tail->next;
	nextTail->prev = tail->prev;
	free(tail);                         // 释放空间内存
	tail = NULL;                        // 手动置为NULL

}



// 将第一个数值为 x 的节点修改
void ListUpdate(const ListNode* pos, const LTDataType x, const LTDataType num)
{
	if (listFind(pos, x) == NULL)
	{
		printf("不用找了 %d，该数值不存在\n", x);
		return;
	}

	ListNode* tail = listFind(pos, x);    // 该节点的地址
	tail->data = num;
}



// 清空链表
void ListDestory(const ListNode* phead)
{
	assert(phead);                    // 断言 头节点不为“空”
	ListNode* cur = phead->next;      // 首节点开始
	
	while (cur != phead)              // 循环遍历
	{
		ListNode* tail = cur->next;
		free(cur);
		cur = tail; // 移动指针
	}
	
	// 最后把首节点也删了
	free(phead);
	phead = NULL;
	printf("删除成功");

}

