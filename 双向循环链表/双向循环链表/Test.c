#define  _CRT_SECURE_NO_WARNINGS  1


#include"List.h"



int main()
{
	Test1();        // 测试方法 一
	Test2();        // 测试方法 二

	return 0;
}

void Test1()
{
	ListNode* pList = ListInit(0);      // 创建定义头节点
	ListPushBack(pList, 9);             // 尾插法
	ListPushBack(pList, 99);            // 尾插法
	ListPushBack(pList, 999);           // 尾插法
	ListPrint(pList);                   // 打印该双向循环链表
	ListPushFront(pList, 100);          // 头插法
	ListPushFront(pList, 10);           // 头插法
	ListPrint(pList);                   // 打印该双向循环链表
	ListPopBack(pList);                 // 尾删法
	ListPopBack(pList);                 // 尾删法
	ListPrint(pList);                   // 打印该双向循环链表
	ListPopFront(pList);                // 头删法
	ListPopFront(pList);                // 头删法
	ListPrint(pList);                   // 打印该双向循环链表

}

void Test2()
{
	ListNode* pList = ListInit(0);      // 创建定义头节点
	ListPushBack(pList, 9);             // 尾插法
	ListPushBack(pList, 99);            // 尾插法
	ListPushBack(pList, 999);           // 尾插法
	ListPrint(pList);                   // 打印该双向循环链表
	ListInsert(pList, 9, 100);          // 在值为 x 的节点前插入数据 
	ListInsert(pList, 999, 1000);       // 在值为 x 的节点前插入数据 
	ListPrint(pList);                   // 打印该双向循环链表
	ListInsert(pList, 9999, 10000);     // 在值为 x 的节点前插入数据 
    ListPrint(pList);                   // 打印该双向循环链表
	ListErase(pList, 9);                // 删除值为 x 的节点
	ListErase(pList, 100);              // 删除值为 x 的节点
	ListPrint(pList);                   // 打印该双向循环链表
	printf("**********************\n");
	ListUpdate(pList, 99, 100);         // 将第一个数值为 x 的节点修改
	ListUpdate(pList, 1000, 9);         // 将第一个数值为 x 的节点修改
	ListPrint(pList);                   // 打印该双向循环链表
	ListDestory(pList);                 // 清空链表
	ListPrint(pList);                   // 打印该双向循环链表
	
	
}