#define  _CRT_SECURE_NO_WARNINGS  1

#include<stdio.h>
#include<time.h>
#include<stdlib.h>


// 打印数组
void playArrays(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);

	}
	
	printf("\n");
}



// 传地址,交换数值
void swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

/* 向下调整算法
前提: 左右子树为堆:  要么左右子树为大堆,要么左右子树为小堆
大堆: 父节点 大于或等于 子节点,   堆顶为最大
小堆: 父节点 大于或等于 子节点,   堆顶为最小
*/





// 建小堆
void AdjustDwonSmall(int* arr, int n, int root)
{
	int parent = root;                  // 父节点的下标位置
	int leftChild = parent * 2 + 1;     // 默认使用左孩子的下标位置

	/* 关于数组下标表示: 父子节点之间的关系
	* 左孩子: leftChild = patent*2 + 1;
	* 右孩子: rightChild = patent*2 + 2;     
	* 对于同一个父节点的左右孩子的关系
	* 右孩子的数组下标位置 = 左孩子的数组下标位置 + 1
	* 父子节点的数组下标位置 =  ( 孩子的数组下标位置 - 1 ) / 2
	*/


	/* 
	* 
	当左孩子的数组下标取值： 越界了说明 到达了叶子节点,不可以再向下调整了
	leftChild = parent*2 +1
	或者
	因为是建小堆,所以当父节点是最小的时候,就不用再向下调整了,跳出循环
	*/

	while (leftChild < n)             
	{ 
		// 判断左右孩子, 取更最小的和父节点的数值交换
		// 同时判断右孩子(leftChild +1)是否存在, 防止右孩子不存在导致数组 越界了
		// 左孩子在循环处判断了,是否会越界的可能

		if (leftChild +1 < n && arr[leftChild + 1] < arr[leftChild])        // arr[leftChild+1] 右孩子的数组的下标位置
		{
			 // 这里判断的是右孩子小, 则把下标 +1 变成右孩子
			leftChild = leftChild + 1;    // 左孩子 + 1 = 右孩子的下标位置

		}

		// 如果孩子数值小于 父节点的数值,交换
		if (arr[leftChild] < arr[parent])
		{
			swap(&arr[leftChild], &arr[parent]);   // 注意传地址,形参的改变影响实参
			 
			parent = leftChild;                    // 继续向下调整,更新父节点, 从下一个更小的数值的孩子的下标位置开始
			leftChild = leftChild * 2 + 1;         // 根据父节点,更新新的左孩子的数组下标位置

		}
		else       // 建小堆,如果 父节点的数值已经是最小的了,就不用再向下调整的            
		{
			       // 跳出循环
			break;
		}
	}

	

}






// 堆排序,降序
void HeapSortDesc(int* arr, int n)
{
	/*
	实际上我们给定的数组一般都不是堆,
	因为不是堆,所以我们无法进行向下调整算法,
	所以我们要先,把堆建起了,这里我们
	建小堆
	从最后一个不是叶子节点的下标位置向上,一点一点的建堆,
	而这最后面不是叶子节点的下标位置等于 : 最后一个叶子节点的父节点
	的 (n-1-1）/2 ， 
	其中的 (n -1)表示的是数组的最后一个数值的下标位置,
	       (n -1 -1) /2 通过孩子计算父节点的数组下标位置的公式
	*/


	for(int i = (n - 1 - 1) / 2; i >= 0; i--)
	{                              // 直到数组下标 0 处, i-- 向上走

		AdjustDwonSmall(arr, n, i);    // 向下调整算法, 从后向上建堆
	}

	/* 堆排序: 降序: 建小堆
	* 小堆: 堆顶最小, 通过从数组堆尾,一步一步的和堆头的最小值交换，从而到达数组的降序的排序
	*/
	int end = n - 1;      // 数组的最后下标位置
	while (end >= 0)
	{
		swap(&arr[0], &arr[end]);         // 尾头交换数值
		AdjustDwonSmall(arr, end, 0);     // 重新调整算法，更新去了最后一个数值的堆
		end--;                            // 继续尾交换后,不再是堆的一部分了,逻辑上移除
	}

}




/*********************************  堆排序, 升序  建大堆******************************************************/
/* 
* 升序,为什建大堆,降序, 为什么建小堆
堆排序其实是选择排序的一种,通过堆来选数

1. 升序, 为什么,建大堆
如果是小堆,最小数在堆顶,已经被选出来了,那么要在剩下的堆数中, 再去选数,但是,剩下的树结构,因为你把堆顶去了,后面的
结构就乱了,不在是大小堆中的任意一个了,所以需要根据剩下的数值,重新建小堆,才能继续选出次小的数值,但是建堆时间复杂度是O(n-1)
那么这样不是不可以而是建堆排序就没有效率优势了,还不如直接遍历选数排序

*/




// 向下调整算法,建大堆,升序
void AdjustDwonBig(int* arr, int n, int root)
{
	int parent = root;              // 父节点数组下标位置
	int leftChild = root * 2 + 1;   // 默认是左孩子数组下标位置

	/* 建大堆: 父节点大于等于左右孩子的数值, 堆顶最大
	1. 当到达叶子节点, 左孩子的数组下标位置大于 数组的长度的时候, 不用再向下调整了
	2. 当父节点的数组下标的数值是最大的时候,也不用再向下调整了,跳出循环
	*/
	while (leftChild < n)
	{
		// 判断左右孩子,那个大, 大得与父节点交换
		/* 注意判断右孩子(leftChild +1)是否存在,防止右孩子不存在,数组越界了
		* 上面循环(while (leftChild < n)) 判断了左孩子是否越界了
		*/
		if (leftChild +1 < n && arr[leftChild + 1] > arr[leftChild])
		{
			// 这里判断的是,右孩子的数值大, +1 把默认的左孩子变成右孩子的下标位置
			leftChild = leftChild + 1;   // 左孩子+1 变右孩子

		}

		// 判断是否比父节点的数值大,大和父节点交换
		if (arr[leftChild] > arr[parent])
		{
			swap(&arr[leftChild], &arr[parent]);   // 注意传地址,形参的改变影响实参

			parent = leftChild;                    // 更新父节点,继续向下调整,以下面的小的左孩子做父节点
			leftChild = parent * 2 + 1;            // 更新左孩子的数组下标,继续向下调整
		}
		else    // 当父节点已经是最大的时候,就不用再向下调整了,跳出循环
		{
			break;
		}
	}
}





// 堆排序,升序
void HeapSortAsc(int* arr, int n)
{
	/*
	* 实际上我们给的数组一般都不是堆,
	* 所以我们需要建堆,升序建大堆，
	* 从最后面不是叶子节点的位置开始调整,一步一步建大堆
	* 该位置的下标等于= 数组最后一个下标位置的父节点
	*  (n-1-1) / 2
	* n-1 是数组的最后一个下标位置
	*/

	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDwonBig(arr, n, i);
	}


	// 堆排序,升序建大堆
	/* 头尾交换
	直到数组 下标 0 处
	*/

	int end = n - 1;  // 数组的最后一个下标位置
	while (end >= 0)
	{
		swap(&arr[0], &arr[end]);    // 堆头尾交换,注意传地址,形参的改变影响实参
		AdjustDwonBig(arr, end, 0);  // 更新去了最后一个数值的堆
		end--;
	}

}




// 堆排序, 测试
void testHeapSort()
{
	int arr[] = { 3,5,2,7,8,6,1,9,4,0 };

	// 堆排序,降序
	printf("堆排序:降序: ");
	HeapSortDesc(arr, sizeof(arr) / sizeof(int));
	playArrays(arr, sizeof(arr) / sizeof(int));
	/*
	*  sizeof(arr) / sizeof(int)  表示计算数组的大小
	* sizeof(arr) : 表示数组的大小, 单位字节
	* sizeof(int):  表示数组元素类型的大小,单位字节
	*/

	// 堆排序,升序
	printf("堆排序:升序: ");
	HeapSortAsc(arr, sizeof(arr) / sizeof(int));
	playArrays(arr, sizeof(arr) / sizeof(int));

}



/*************************** 直接选择排序            ****************************************/

// 直接选择排序,升序
void selectSortAsc(int* arr, int n)
{
	int begin = 0;      // 数组最开头的下标位置
	int end = n - 1;    // 数组最后面的下标位置

	// 一头一尾, 交换
	while (begin < end)
	{
		int min = begin;     // 数组中最小值的下标位置 
		int max = begin;     // 数组中最大值的下标位置, 默认最开始数组最大最小的数值的下标位置都是 0 ,用于比较数组的的数值

		for (int i = begin; i <= end; i++)         // 注意这里是 i = begin 不是 0 因为是一步一步向上走的
		{
			// 找到数组中最小的数值的下标位置
			if (arr[min] > arr[i])
			{
				min = i;
			}

			// 找到数组中最大的数值的下标位置
			if (arr[max] < arr[i])
			{
				max = i;
			}
		}

		// 跳出循环,数组的最大值,最小值的下标位置都找到了,交换一头一尾

		swap(&arr[min], &arr[begin]);     // 最小值,放到数组最前的位置 begin 

		if (begin == max)      // 需要修正一下 max的位置
		{
			max = min;         // 因为当优先交换的缘故，当 begin = max 的时候,
			                   // max 最大值的下标位置会被改变到min的位置处了,所以需要修正
		}

		swap(&arr[max], &arr[end]);    // 最大值,放到数组最后面的位置 end

		end--;       // 下标前移
		begin++;     // 下标后移

	}

}





// 直接排序,降序
void SelectSortDesc(int* arr, int n)
{
	int begin = 0;
	int end = n - 1;

	while (begin < end)
	{
		int max = begin;      // 设置默认最大值的下标
		int min = begin;      // 设置默认最小值的下标位置, 无论是最大还是最小值的下标默认设置都为数组的 0 下标位置，比较好比较判断

		for (int i = begin; i <= end; i++)
		{
			// 找到 数组中的最小值的下标位置
			if (arr[min] > arr[i])
			{
				min = i;
			}


			// 找到数组中的最大值的下标位置
			if (arr[max] < arr[i])
			{
				max = i;
			}
		}

		// 找到其数组中的最大值和最小值跳出循环
		/* 降序
		把数值大的放到数组的前面
		数值小的放到数组的后面
		*/

		swap(&arr[min], &arr[end]);    // 把数值小的放到数组的后面, 注意使用传地址的方式,形参的改变影响实参
		if (max == end)                // 当最大值在 end 的位置的时候,会被 前面的交换改变下标位置的
		{                              // 改变到了min 的下标位置所以我们需要修正一下
			max = min;
		}
		swap(&arr[max], &arr[begin]);  // 把数值大的放到数组的前面,注意传地址,形参的改变影响实参

		begin++;   // 后移动
		end--;     // 前移动
	}
}



// 直接选择排序,测试
void testSelectSort()
{
	int arr[] = { 0,5,2,7,8,6,1,9,4,0 };

	printf("直接选择排序:升序: ");
	selectSortAsc(arr, sizeof(arr) / sizeof(int));
	playArrays(arr, sizeof(arr) / sizeof(int));

	printf("直接选择排序:降序: ");
	SelectSortDesc(arr, sizeof(arr) / sizeof(int));
	playArrays(arr, sizeof(arr) / sizeof(int));

}

/* 直接选择排序，是最差的排序算法, 最坏的情况, 时间复杂度为O(N*N)
* 最差的原因是: 无论是最好的情况,本身该数组就是有序的,其排序的时间复杂度还是 O(n*n)
* 就是说无论是最好的情况,还是最坏的情况时间复杂度都为O(n*n)
*/


// 堆排序,选择排序 的性能测试
void Testop()
{
	srand(time(0));         // 设定时间种子

	const int N = 100000;   // 设置数组的大小

	int* a = (int*)malloc(sizeof(int) * N);    // 堆区上开辟 10w 个空间
	int* b = (int*)malloc(sizeof(int) * N);    // 堆区上开辟 10w 个空间

	// 判断动态(堆区) 上开辟的空间是否成功
	if (NULL == a || NULL == b)
	{
		perror("malloc error");                // 提示错误
		exit(-1);                              // 非正常退出程序
	}

	for (int i = 0; i < N; i++)
	{
		a[i] = rand();                         // 生成随机值
		b[i] = a[i];                           // 赋值,让两个数组中的值一样,控制变量
	}

	int begin1 = clock();                      // 直接选择排序,运行前的时间点
	selectSortAsc(a, N);                       // 直接选择排序,升序
	int end1 = clock();                        // 直接选择排序,运行结束后的时间点


	int begin2 = clock();                      // 堆排序,运行前的时间点
	HeapSortAsc(a, N);                         // 堆排序,升序
	int end2 = clock();                        // 堆排序,运行结束后的时间点

	/* 
	* 排序的运行时间 = （排序运行结束后的时间点 end) - (排序运行前的时间点 begin) 
	*/

	printf("直接选择排序: ");
	printf("%d\n", end1 - begin1);
	printf("堆排序: ");
	printf("%d\n", end2 - begin2);
}


int main()
{
	// 堆排序测试
	 testHeapSort();

	// 直接排序测试
	 testSelectSort();


	// 直接选择排序, 堆排序的性能测试
	Testop();

	return 0;
}


