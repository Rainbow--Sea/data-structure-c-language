//#define  _CRT_SECURE_NO_WARNINGS  1

#include"Queue.h"


int main()
{
	test();

	return 0;
}



// 队列测试
static void test()
{
	Queue q;                         // 定义队列
	QueueInit(&q);                   // 初始化队列
	QueuePush(&q, 3);                // 队尾，入队
	QueuePop(&q);                    // 队头，出队
	QueuePush(&q, 6);                // 队尾，入队
	QueuePush(&q, 9);                // 队尾，入队
	QueuePush(&q, 99);                // 队尾，入队
	QueuePush(&q, 10);                // 队尾，入队
	QueuePush(&q, 100);                // 队尾，入队
	printf("%d\n", QueueFront(&q));    // 取队头数据
	printf("%d\n", QueueEmpty(&q));    // 判断队列是否为空 NULL
	printf("%d\n", QueueBack(&q));     // 取队尾数据
	printf("%d\n", QueueSize(&q));     // 计算队列中存在多少个数据


	while (!QueueEmpty(&q))    // 判断队列是否为空.队列为空，返回 true,就是数值上的 1,非空，返回false,就是数值上的 0，
	{                          // 所以这里需要 取非一下
		printf("%d ", QueueFront(&q));   // 取队头数据
		QueuePop(&q);                    // 队头，出队
		/*
		* 注意队列和栈一样，栈是，取栈顶元素，后必须紧接着栈顶出栈，不然你是无法取到后面的数据的
		* 而队列也是一样的，队列是，取队头数据，后必须紧接着队头出队，不然你是无法取到后面的数据的
		*/

	}
	printf("\n");
	 


	QueueDestory(&q);     // 删除队列
}