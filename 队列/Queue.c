//#define  _CRT_SECURE_NO_WARNINGS  1
#include"Queue.h"




// 初始化队列
void QueueInit(Queue* pq)
{
	pq->head = NULL;
	pq->tail = NULL;

}



// 删除队列
void QueueDestory(Queue* pq)
{
	assert(pq);   // 断言，队列不为空(NULL)

	QNode* cur = pq->head;    // 拷贝队头的地址，用于移动

	while (cur)
	{
		QNode* next = cur->next;   // 拷贝下一个节点的地址，用于释放空间，防止丢失整个队列
		free(cur);   // 释放队头节点的空间
		cur = next;  // 移动节点
	}

	// 同时把队头队尾的空间释放掉，并置为 NULL,防止非法访问
	free(pq->head);
	pq->head = NULL;
	free(pq->tail);
	pq->tail = NULL;

}



// 队尾，入队
/*
* 两种情况
* 1. 首个数据入队
* 2. 存在多个数据入队
*/
void QueuePush(Queue* pq, const QDataType x)
{
	assert(pq);    // 队列不为 NULL

	QNode* newNode = (QNode*)malloc(sizeof(QNode));    // 注意可以的类型是队列的结构体类

	if (NULL == newNode)               // 判断开辟空间是否成功
	{
		perror("创建节点失败");
		exit(-1);    // 非正常结束程序
		
	} 
	else         // 否则创建节点成功，赋值初始化
	{
		newNode->data = x;
		newNode->next = NULL;
	}

	if (pq->head == NULL)    // 第一个数据，队尾入队
	{
		pq->head = newNode;       // 第一个数据，队尾，队头都是指向同一个节点的地址的
		pq->tail = newNode;
	}
	else                      // 存在多个数据，队尾入队
	{
		pq->tail->next = newNode;     // 插入节点
		pq->tail = newNode;           // 插入数据后，重新定义队尾的位置
	}

}


/*
// 队头，出队
void QueuePop(Queue* pq)
{
	assert(pq);
	assert(pq->head);   // pq->head 首节点不为空
	
	Queue* next = pq->head ->next;  // 拷贝保存到首节点下一个节点的地址，防止在释放空间上，丢失了整个队列
	free(pq->head);     // 释放首节点的空间
	pq->head = next;    // 重新指向新的首节点
	/*但是这里存在一个问题就是
	当出队时只有一个节点时，
	head 头为空的 队头出队为空的话，没有关系会判断断言
	但是队尾 tail， 尾节点释放了空间，但是没有置为 NULL,还会访问该空间，但是这个空间
	已经不是它的了，这是非法访问了，非法访问，是会出错的，
	所以我们需要将尾节点置为空NULL,防止被访问，

}*/
// 优化
// 队头，出队
void QueuePop(Queue* pq)
{
	assert(pq);         // 断言 队列不为空(NULL)
	assert(pq->head);   // pq->head 首节点不为空(NULL)

	if (NULL == pq->head->next)    // 为空的话，表示只剩下，最后一个尾节点了
	{
		free(pq->tail);
		pq ->head = NULL;
		pq->tail = NULL;   // 首节点和尾节点都置为NULL

	}
	else   // 存在多个节点拷贝释放
	{
		QNode* next = pq->head->next;  // 拷贝保存到首节点下一个节点的地址，防止在释放空间上，丢失了整个队列
		free(pq->head);     // 释放首节点的空间
		pq->head = next;    // 重新指向新的首节点
	}

	
}





// 取队头数据
QDataType QueueFront(const Queue* pq)
{
	assert(pq);         // 队列不为空(NULL)
	assert(pq->head);   // 队头不为空(NULL)

	return pq->head->data;   // 返回队头数据
}



// 取队尾数据
QDataType QueueBack(const Queue* pq)
{
	assert(pq);          // 队列不为空 (NULL)
	assert(pq->tail);    // 队尾不为空 (NULL)

	return  pq->tail->data;    // 返回队尾数据
}



// 计算队列中存在多少元素
int QueueSize(const Queue* pq)
{
	assert(pq);    // 断言 队列不为空(NULL)
	assert(pq->head);  // 断言  队头也不为空(NULL)

	int count = 0;     // 计数
	QNode* cur = pq->head;    // 拷贝队头，代替队头移动，防止丢失整个队列

	while (cur)       // 队头不为空，
	{
		count++;
		cur = cur->next; // 移动节点
	}

	return count;     // 返回计数结果
}



// 判断队列是否为空队列
bool QueueEmpty(const Queue* pq)
{
	assert(pq);    // 断言 队列不为空(NULL)

	return NULL == pq->head;   // 判断一个队列是否为空，判断一下首节点是否为空就可以了，空,返回 true, 非空,返回 false
}
