#define  _CRT_SECURE_NO_WARNINGS  1

#include"Stack.h"

// 打印数组
void playPrint(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}

	printf("\n");
}



// 交换数值
void swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}


// 三数取中法
int getMidIndex(int* arr, int left, int right)
{
	int mid = (left + right) >> 1;   // 相等于 (left + right) / 2

	if (arr[left] < arr[mid])
	{
		if (arr[mid] < arr[right])
		{   // arr[left] < arr[mid] < arr[right] mid 为中间值下标
			return mid;
		}
		else if (arr[right] < arr[left])
		{   // arr[right] < arr[left] < arr[mid] left 为中间值下标
			return left;
		}
		else   // arr[right] > arr[left]
		{   // arr[left] < arr[right] < arr[mid]  right 为中间值下标
			return right;
		}
	}
	else     // arr[left] > arr[mid]
	{
		if (arr[mid] > arr[right])
		{   // arr[left] > arr[mid] > arr[right]  mid 为中间值下标
			return mid;
		}
		else if (arr[right] > arr[left])
		{  // arr[right] > arr[left] > arr[mid]  left 为中间值下标
			return left;
		}
		else   // arr[left] > arr[right] 
		{      // arr[left] > arr[right] > arr[mid]  right 为中间值下标
			return right;
		}
	}
}



// 单趟 key 分界 挖坑法,升序
int parSortAsc(int* arr, int left, int right)
{
	int index = getMidIndex(arr, left, right);   // 三数取中,防止key 为最大/最小值,导致O(n*n)
	swap(&arr[left], &arr[right]);               // 三数取中和数组首元素交换,用于后面的取值

	int begin = left;    // 左边前下标
	int end = right;     // 右边后下标
	int pivot = begin; 
	int key = arr[pivot];   // 坑位,关键值的数值

	while (begin < end)
	{
		// 优先从右边开始,升序找小于 key 的数值的下标
		while (begin < end && arr[end] >= key)  // begin < end 防止在该内循环中就已经 begin 与 end相遇了,而没有跳出
		{                     // >= 需要 = ,如果没有 = 就会导致当 arr[end] = key 时,不会执行 end--
			end--;            // 导致 end 停留在 相等的下标位置,begin 与 end 无法相遇,导致死循环
		}
		// 跳出循环找到了, 小于 key 的数值下标
		arr[pivot] = arr[end];  // 填入到左边的坑位中
		pivot = end;            // 自己成为新的坑位

		// 左边,升序,找大于 key 的数值下标
		while (begin < end && arr[begin] <= key)  // begin < end <= 与上面同理
		{
			begin++;
		}
		// 跳出循环找到了,大于 key 的数值下标
		arr[pivot] = arr[begin]; // 填入右边的坑位中
		pivot = begin;           // 自己成为新的坑位

	}

	// 相遇跳出循环,将 key 填入到相遇点
	pivot = begin;   // 相遇点,成为坑位 begin = end
	arr[pivot] = key;  // key 排序好了

	return pivot;  // 返回排序好的下标位置,不要动
}




// 单躺 key 分界 挖坑法,降序
int parSortDesc(int* arr, int left, int right)
{
	int index = getMidIndex(arr, left, right);   // 三数取中,防止key关键值为 最大值/ 最小值,O(n*n)
	swap(&arr[left], &arr[index]);               // 三数取中的数值和数组首元素交换,用于后面的操作

	int begin = left;     
	int end = right;
	int pivot = begin;
	int key = arr[pivot];   // 关键值

	while (begin < end)
	{
		// 优先从右边开始,找大于 key 的数值的下标,
		while (begin < end && arr[end] <= key)   // begin < end 防止在该内循环中已经 begin 与 end相遇,没有跳出
		{                     // <= 需要 = ,如果没有 = ,当 arr[end] = key 时, 就不会执行 end--,
			end--;            // begin 与 end 就不会相遇,导致死循环.
		}
		// 退出循环找到了, 大于 key 的数值下标
		arr[pivot] = arr[end]; // 将大于 key 填入左边坑位
		pivot = end;     // 自己成为新的坑位

		// 左边找小于 key 的数值下标
		while (begin < end && arr[begin] >= key)  // begin < end >= 与上面同理
		{
			begin++;
		}

		// 退出循环找到了,小于 key 的数值下标
		arr[pivot] = arr[begin];  // 将小于key 的数值,填入到右边坑位
		pivot = begin;   // 自己成为新的坑位
	}

	// 相遇跳出循环
	pivot = begin;   // 相遇点,作为坑位,begin = end
	arr[pivot] = key; // 将 key 填入坑位,排序好了

	return pivot;  // 将排序好的数值下标返回,不要动。
}

/*  递归缺陷: 栈帧深度太深了,栈空间不够用,可能会溢出, 栈程序时没有错的
* 递归改非递归的方式：
* 1.直接改循环(斐波那契额数列)
* 2.借助自己或库中的栈,模拟实现栈中的递归过程,数据的入栈出栈
*/

// 快速排序非递归实现, 升序
void quickSortNorAsc(int* arr, int n)
{
	ST st;   // 定义栈，
	StackInit(&st);     // 初始话栈

	StackPush(&st, n - 1);  // 右边入栈
	StackPush(&st, 0);      // 左边入栈, 先进后出

	while (!StackEmpty(&st))   // 栈不为空
	{
		int left = StackTop(&st);  // 取栈顶元素
		StackPop(&st);             // 出栈,注意取栈顶元素 和 出栈是一起的,不然你无法取到后面的数据

		int right = StackTop(&st);
		StackPop(&st);
		
		int keyIndex = parSortAsc(arr, left, right);   // 排序好的数值下标

		// [left, keyIndex-1] keyIndex [keyIndex+1, right]  keyIndex-1 是排序好的,不要动

		if (keyIndex + 1 < right)   // 右区间的数值小于 right 则还有没有排序好,
		{
			StackPush(&st, right);  // 右区间的右区间入栈
			StackPush(&st, keyIndex + 1);   // 右区间的左区间入栈
		}

		if (left < keyIndex - 1)    // 左区间的数值 大于 left 还没有排序好,需要继续排序
		{
			StackPush(&st, keyIndex - 1);  // 左区间的右区间入栈
			StackPush(&st, left);          // 左区间的左区间入栈
		}
	}


	StackDestory(&st);  // 销毁栈
}



// 快速排序,非递归,降序‘
void quickSortNorDesc(int* arr, int n)
{

	ST st;  // 定义栈
	StackInit(&st);   // 初始化栈

	StackPush(&st, n - 1);  // 右边先入栈
	StackPush(&st, 0);      // 左边再入栈

	while (!StackEmpty(&st))  // 栈不为空
	{
		int left = StackTop(&st);  // 取栈顶
		StackPop(&st);    // 出栈,取栈顶和出栈要一起,不然取不到后面的数据

		int right = StackTop(&st);  // 取栈顶
		StackPop(&st);              // 出栈,取栈顶和出栈要一起,不然取不到后面的数据

		int keyIndex = parSortDesc(arr, left, right); // 排序好,的数值下标位置

		// [left, keyIndex-1] keyIndex [keyIndex+1, right]  keyIndex-1 是排序好的,不要动

		if (keyIndex + 1 < right)   // 右区间中还没有排序好,继续入栈排序
		{
			StackPush(&st, right);   // 右区间的右区间入栈
			StackPush(&st, keyIndex + 1);    // 右区间的左区间入栈
		}

		if (left < keyIndex - 1)      // 左区间还没有排序好,需要继续调整
		{
			StackPush(&st, keyIndex - 1);  // 左区间中的右区间入栈
			StackPush(&st, left);          // 左区间中的左区间入栈
		}

	}


	StackDestory(&st);  // 销毁栈
}


// 测试快速排序,非递归
void testQuickSort()
{
	int arr[] = { 6,1,2,7,9,3,4,5,10,8 };
	//int i = parSortAsc(arr, 0, sizeof(arr) / sizeof(int) - 1);  // 单趟 key 分界,升序

	quickSortNorAsc(arr, sizeof(arr) / sizeof(int));   // 快速排序非递归的升序
	playPrint(arr, sizeof(arr) / sizeof(int));
	//parSortDesc(arr,0, sizeof(arr) / sizeof(int) - 1); // 单趟 key分界,降序

	quickSortNorDesc(arr, sizeof(arr) / sizeof(int));  // 快速排序非递归的降序
	playPrint(arr, sizeof(arr) / sizeof(int));

}

int main()
{
	testQuickSort();

	return 0;
}