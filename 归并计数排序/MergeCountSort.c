#define  _CRT_SECURE_NO_WARNINGS  1

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

// 打印数组
void playPrint(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}

	printf("\n");
}



// 归并排序,升序
void _mergeSortAsc(int* arr, int left, int right, int* tmp)
{
	if (left >= right)    // 递归结束条件: 当分到只有一个数值时(left = right) 的时候就有序了,返回归并,拼接
	{                     // 当 left > right 左边序列 大于 右边序列 该序列不存在返回
		return;
	}

	int mid = (left + right) >> 1;   // 除 2 取中间下标

	// 假设 [left, mid] [mid+1, right] 有序,那么我们就可以归并

	_mergeSortAsc(arr, left, mid, tmp);        // 左序列,分序归并
	_mergeSortAsc(arr, mid + 1, right, tmp);   // 右序列,分序归并

	// [left, mid]
	int begin1 = left, end1 = mid;         // 左序列中的起始位置和终止下标
	int begin2 = mid + 1, end2 = right;    // 有序列中的起始位置和终止下标位置

	int index = left;   // 记录到,数组的起始位置

	while (begin1 <= end1 && begin2 <= end2)    // 如果存在左右序列中其中一个序列,比较归并结束了,就
	{                                           // 把剩下那个也归并,拼接上.
		if (arr[begin1] < arr[begin2])          // 左序列,小的数值入临时数组中
		{
			tmp[index++] = arr[begin1++];           // 赋值入临时数组,注意是后置加加
		}
		else      // begin2 小入临时数组
		{
			tmp[index++] = arr[begin2++];         // 赋值入临时数组,注意是后置加加
		}

	}

	// 跳出循环 说明其中的左右序列中有一个已经,通过比较入临时数组结束了
	while (begin1 <= end1)   
	{ // 设定 begin1 没有结束 入临时数组

		tmp[index++] = arr[begin1++];   // 注意是后置++
	}

	while (begin2 <= end2)
	{  // 假设 begin2 没有结束 入临时数组
		tmp[index++] = arr[begin2++];  // 注意是后置++
	}

	// 临时数组拷贝到真正的数组中去,  // left 数组的起始位置,right 数组的最后的位置
	for (int i = left; i <= right; i++)
	{
		arr[i] = tmp[i];     // 逐一拷贝
	}

}



// 归并排序,降序
void _mergeSortDesc(int* arr, int left, int right, int* tmp)   // int*tmp 临时数组
{
	if (left >= right)   // 递归结束条件,当只有一个数值时 left = right 有序递归返回
	{                    // left > right 左边大于右边 不存在该序列返回
		return;
	}

	int mid = (left + right) >> 1;   // / 2 得中间下标位置

	// 假设 [left, mid] [mid+1, right] 有序,那么我们可以归并了
	_mergeSortDesc(arr, left, mid, tmp);   // 左序列,分序归并
	_mergeSortDesc(arr, mid + 1, right, tmp);     // 右序列,分序归并

	int begin1 = left, end1 = mid;        // 左序列 保存
	int begin2 = mid + 1, end2 = right;   // 右序列 保存

	int index = left;   // 数组起始位置,复制,用于 临时数组的赋值拷贝

	while (begin1 <= end1 && begin2 <= end2)   // 左右序列存在一个比较归并完了,跳出循环,将剩下一个没有归并的拼接归并
	{
		if (arr[begin1] > arr[begin2])       // 降序, begin1 左序列数值大
		{
			tmp[index++] = arr[begin1++];  // 归并到临时数组,后置++
		}
		else     // 降序, begin2 右序列数值大
		{
			tmp[index++] = arr[begin2++];   // 归并到临时数组,后置++
		}
	}

	// 跳出循环,将剩下一个没有比较归并完的数组,归并到后面
	while (begin1 <= end1)    // 这里假设 左序列 begin1 没有归并完
	{
		tmp[index++] = arr[begin1++];   // 归并到临时数组中去 后置++
	}

	while (begin2 <= end2)    // 这里假设 右序列 begin2 没有归并完
	{
		tmp[index++] = arr[begin2++];    // 归并到临时数组中去 后置++
	}

	// 拷贝将临时数组中的排序好的数值, 赋值,覆盖到真正的数组当中去
	for (int i = left; i <= right; i++)
	{
		arr[i] = tmp[i];   // 赋值,覆盖数组
	}

}



void mergeSortDesc(int* arr, int n)
{
	// 开辟和真正需要排序的数组一样大小的临时数组,用于存放归并,合并 连接排序好的数组
	int* tmp = (int*)malloc(sizeof(int) * n);

	// 判断堆区上开辟的空间是否成功,
	if (NULL == tmp)
	{
		perror("malloc error");   // 提示错误
		exit(-1);                 // 非正常结束程序
	}

	_mergeSortDesc(arr, 0, n - 1, tmp);   //  归并排序,降序

	free(tmp);  // 释放堆区上开辟的空间
	tmp = NULL; // 置为 NULL, 防止非法访问

}



// 归并排序,降序
void mergeSortAsc(int* arr, int n)
{
	// 开辟和真正需要排序的数组一样大小的临时数组,用于存放归并合并连接排序好的数组
	int* tmp = (int*)malloc(sizeof(int) * n);

	// 判断开辟的堆区上开辟的空间是否,成功
	if (NULL == tmp)
	{
		perror("malloc error");   // 失败,
		exit(-1);                 // 程序非正常结束
	}

	_mergeSortAsc(arr, 0, n - 1, tmp);    // 归并排序,升序

	free(tmp);   // 释放该开辟的空间
	tmp = NULL;  // 置为 NULL, 防止非法访问

}



// 归并排序,非递归实现,升序
void mergeSortNonRAsc(int* arr, int n)
{
	// 动态创建一个和 主数组一样的临时数组
	int* tmp = (int*)malloc(sizeof(int) * n);

	// 判断动态开辟的数组是否成功
	if (NULL == tmp)
	{
		perror("malloc error");   // 错误提醒
		exit(-1);                 // 非正常退出程序
	}

	//memset(tmp, 0, sizeof(int) * n);

	int gap = 1;       // 每组数据个数,从 1 开始

	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)     
		{
			// 假定 [i,i+gap-1] [i+gap,i+2*gap-1]
			int begin1 = i, end1 = i + gap - 1;          // 左序列范围
			int begin2 = i + gap, end2 = i + 2 * gap - 1;  // 右序列范围

			if (begin2 >= n)    // 归并的过程中右半区间,可能不存在
			{
				break;          // 退出,只有左半区间了,而左半区间我们已经排序好了
			}

			if (end2 >= n)      // 归并过程中右半区间,算多了,修正一下
			{
				end2 = n - 1;   // 修正到最后一个数组的位置
			}

			int index = i;      // 保存 i，用于保存

			while (begin1 <= end1 && begin2 <= end2)   // 当存在一个左右序列其中一个排序归并完了,把剩下的那个没有结束的拼接了
			{
				if (arr[begin1] < arr[begin2])     // begin1 下标的数值小,先入临时数组中
				{
					tmp[index++] = arr[begin1++];    // 填入临时数组中去
				}
				else      // begin2 下标的数值小,先入临时数组中
				{
					tmp[index++] = arr[begin2++];    // 填入临时数组中去
				}
			}

			// 跳出循环,有一个序列已经排序归并结束了,剩下那个没有的拼接上
			while (begin1 <= end1)     // 左序列没有结束
			{
				tmp[index++] = arr[begin1++];
			}

			while (begin2 <= end2)     // 右序列没有结束
			{
				tmp[index++] = arr[begin2++];
			}


			// 拷贝回去，将临时数组排序好的数值拷贝到真正的数组中去
			for (int j = i; j <= end2; j++)
			{
				arr[j] = tmp[j];    // 赋值,覆盖数组
			}
		}

		gap *= 2;     // gap 的变化 扩大

	}

	free(tmp);        // 释放动态空间
	tmp = NULL;       // 手动置为 NULL，防止越界访问
}



// 归并排序,降序,非递归实现
void mergeSortNonRDesc(int* arr, int n)
{
	// 拷贝临时数组
	int* tmp = (int*)malloc(sizeof(int) *n);

	if (NULL == tmp)
	{
		perror("malloc error");
		exit(-1);
	}

	int gap = 1;     // 分组数据从 1 个开始

	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			// 假设 [i,i+gap-1] [i+gap,i+2*gap-1]
			int begin1 = i, end1 = i + gap - 1;           // 左序列,分组
			int begin2 = i + gap, end2 = i + 2 * gap - 1; // 右序列,分组

			// 归并过程中,可能右半区间不存在,
			if (begin2 >= n)
			{
				break;   // 右半区间,不存在（就不用排序了), 直接返回,左序列已经排序好了,
			}

			// 归并过程中右半区间算多了,修正一下
			if (end2 >= n)
			{
				end2 = n - 1; // 修正到最后一个数组的下标位置
			}

			int index = i;    // 保存用于,拷贝到真正数组中

			// 当左右序列有其中一个归并排序好了,把剩下的那个没有的好的归并拼接上
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (arr[begin1] > arr[begin2])     // 左begin1对应的下标数值大,先归并到临时数组中去
				{
					tmp[index++] = arr[begin1++];  // 赋值,覆盖到临时数组中
				}
				else    // 右 begin2 对应的下标数值大,先归并到临时数组中去
				{
					tmp[index++] = arr[begin2++];
				}

			}

			// 跳出循环,左右序列中有一个排序归并好了,将剩下的那个没有好的,归并上
			while(begin1 <= end1)     // 左序列没有结束
			{
				tmp[index++] = arr[begin1++];
			}

			while (begin2 <= end2)    // 右序列没有结束
			{
				tmp[index++] = arr[begin2++];
			}

			// 将临时排序好的数组 拷贝复制到 真正的数组中去
			for (int j = i; j <= end2; j++)
			{
				arr[j] = tmp[j];
			}
		}

		gap *= 2;   // gap 每次扩展 2 倍
	}

	free(tmp);   // 释放空间
	tmp = NULL;  // 置为 NULL,防止非法越界

}



/*
* 非比较排序,计数排序
* 计数排序：思想很巧,适用的范围具有局限性,当(最大值与最小值的差别不要太大,不然很浪费空间
* 时间复杂度:O(n+range) 说明适用于排序范围集中一组整形数据排序
* 空间复杂度:O(range) 
*/


// 计数排序
void countSortAsc(int* arr, int n)
{
	int max = arr[0];
	int min = arr[0];

	for (int i = 0; i < n; i++)
	{
		if (arr[i] > max)    // 找数组中的最大值
		{
			max = arr[i];
		}

		if (arr[i] < min)    // 找数组中的最小值
		{
			min = arr[i];
		}
	}

	int range = max - min + 1;   // 计算数组的大小 [0~9] = 9-0+1=10

	// 堆区上开辟更具计算的相对映射空间 计算数组的大小
	int* count = (int*)malloc(sizeof(int) * range);
		
	// 判断开辟的空间是否成功了
	if (NULL == count)
	{
		perror("malloc error");    // 提醒错误
		exit(-1);                  // 程序非正常结束
	}

	memset(count, 0, sizeof(int) * range);    // 将数组所有的元素初始化为 0

	// 统计数值个数
	for (int i = 0; i < n; i++)
	{
		count[arr[i] - min]++;    // 相对映射的下标位置的统计数值
	}

	// 根据统计的数值我们进行排序
	for (int i = 0, j = 0; i < range; i++)
	{
		while (count[i]--)
		{
			arr[j++] = i + min;   // 赋值上相对映射的实际数值 +min
		}
	}
	

	free(count);   // 释放空间
	count = NULL;  // 置为NULL,防止非法访问

}


// 计数排序,降序
void countSortDesc(int* arr, int n)
{
	int max = 0;
	int min = 0;

	for (int i = 0; i < n; i++)
	{
		if (arr[i] < min)
		{
			min = arr[i];
		}

		if (arr[i] > max)
		{
			max = arr[i];
		}

	}

	int range = max - min + 1;   // 数组的大小[0~9] = 9-0+1=10

	int* count = (int*)malloc(sizeof(int) * range);    //创建一个临时数组

	// 判断堆区上开辟的空间是否成功
	if (NULL == count)
	{
		perror("malloc error");
		exit(-1);
	}

	memset(count, 0, sizeof(int) * range);    // 将临时数组中的全部元素初始化为 0

	// 统计个数
	for (int i = 0; i < n; ++i)
	{
		count[arr[i] - min]++;    // 相对映射的下标位置 i-min
	}

	// 根据统计的个数,将数组中的数值排序好
	for (int j = 0, i = range - 1; i >= 0; i--)   // i = range-1 是数组的最后一个下标位置
	{
		while (count[i]--)
		{
			arr[j++] = i + min;   // 注意是相对位置之间的映射

		}
	}

	
}





void testMergeSort()
{
	int arr[] = { 10,6,1,3,9,4,2,0,99,100 };

	countSortDesc(arr, sizeof(arr) / sizeof(int));     // 计数排序,降序

	//countSortAsc(arr, sizeof(arr) / sizeof(int));         // 计数排序,升序

    //mergeSortNonRDesc(arr, sizeof(arr) / sizeof(int));   // 归并排序,降序,非递归排序

	// mergeSortNonRAsc(arr, sizeof(arr) / sizeof(int));  // 归并排序,升序,非递归排序

	// mergeSortDesc(arr, sizeof(arr) / sizeof(int));  // 归并排序,降序
	// mergeSortAsc(arr, sizeof(arr) / sizeof(int));   // 归并排序,升序
	playPrint(arr, sizeof(arr) / sizeof(int));      // 打印数组

	/*
	* sizeof(arr)/sizeof(int) 数组的大小
	* sizeof(arr) 表示数组的所占空间的大小, 单位字节
	* sizeof(int) 表示数组元素类型所占空间的大小, 单位字节
	*/

}





int main()
{
	testMergeSort();

	return 0;
}