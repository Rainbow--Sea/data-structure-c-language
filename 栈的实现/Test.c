#define  _CRT_SECURE_NO_WARNINGS  1

#include "Stack.h"


int main()
{
	ST st;  // 定义创建栈(结构体)的类型
	TestStack(&st);  // 注意，形参改变实参，传地址

	return 0;
}



// 栈的测试函数
void TestStack(ST* st)
{
	StackInit(st);  // 初始化栈

	StackPush(st, 1);
	StackPush(st, 2);    // 数值的入栈
	StackPush(st, 3);
	StackPush(st, 4);
	StackPush(st, 5);
	StackPush(st, 6);
	StackPush(st, 7);


	printf("%d\n", StackTop(st));  // 取栈顶元素
	StackPop(st);                  // 栈顶出栈，

	/*
	* 注意只有栈顶一端才可以入栈出栈
	* 也只有把后面的出栈了，才可以弹出前面的数值，先进后出
	*/

	printf("判断栈是否为空: %d\n", StackEmpty(st));        // 判断栈是否为 空栈
	printf("该栈的有效数据个数:%d\n", StackSize(st));  // 求栈的元素个数


	printf("%d\n", StackTop(st));  // 取栈顶元素
	StackPop(st);                  // 栈顶出栈，

	while (!StackEmpty(st))   // 这里使用!运算符，因为当栈为 0 返回值是 true,栈不为 0，返回值是 false,需要进入循环需要 !(非一下)     
	{
		// 循环遍历取出栈顶元素
		printf("%d\n", StackTop(st));  
		StackPop(st);     // 出栈
	}


	StackDestory(st);  // 销毁栈
}

