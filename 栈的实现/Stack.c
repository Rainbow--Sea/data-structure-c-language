#define  _CRT_SECURE_NO_WARNINGS  1

#include"Stack.h"


/*
* 初始化栈
*/
void StackInit(ST* st)
{
	assert(st); // 断言，判断st 不为 NULL；

	st->data = (STDatatype*)malloc(sizeof(STDatatype) * OCE);

	// 判断动态内存(堆上)开辟的空间是否成功
	if (NULL == st->data)
	{
		perror("st->data 动态开辟空间");
		exit(-1);  // 非正常中止程序
	}
	
	st->copacity = 2;   // 当前栈的容量
	st->top = 0;        // 栈顶位置

	/*
	* top = 0 ,表示的是栈顶的位置是该最后元素的下一个元素的位置
	* top = 1; 表示的是栈顶的位置就是该最后元素的位置
	*/
}



/*
* 销毁栈
*/
void StackDestory(ST* st)
{
	assert(st);   // 断言，st是否为空

	free(st->data);  // 释放该内存空间
	st->data = NULL; // 手动置为NULL，防止非法访问
	st->copacity = 0;
	st->top = 0;

}



// 栈顶入栈操作
void StackPush(ST* st, STDatatype x)
{
	assert(st);  // 断言判断,st是否为NULL

	// 判断栈是否满了，满了扩容
	if (st->copacity == st->top)
	{
		// 动态(堆区)扩容开辟空间
		STDatatype* tmp = (STDatatype*)realloc(st->data, st->copacity * OCE * sizeof(STDatatype));

		// 判断动态内存开辟空间是否成功
		if (NULL == tmp)
		{
			perror("tmp 扩容");      // 打印错误报告
			exit(-1);                // 非正常退出
		}
		else
		{
			// 动态开辟空间成功,转移主权
			st->data = tmp;
			st->copacity *= OCE;  // 栈容量扩张
		}
	}

	st->data[st->top] = x;    // 栈顶入栈数值
	st->top++;                // 栈顶加加

}



/*
* 栈顶出栈操作
*/
void StackPop(ST* st)
{
	assert(st);          // 断言判断，st 是否为 NULL

	assert(st->top > 0);   // 出栈的位置必须大于 0 ，不能一直删除没了吧

	st->top--;           // 栈顶位置减减一下，就好了
	/*
	* 注意不可以把 st->data[st->top] = 0; 置为 0 的方式
	* 因为可能你存放的数值就是 0 呢，不就有误导了吗
	*/
}



/*
* 取出栈顶元素
*/
STDatatype StackTop(ST* st)
{
	assert(st);  // 断言判断，st 是否为NULL

	assert(st->top > 0);  // 栈不为空

	return st->data[st->top-1];
	/*
	* -1 ，是因为这里的top 初始化是 0 
	* 其top所指向的位置是 该最后一个元素的下一个位置
	*/
}



/*
* 求出栈顶元素的个数
*/
int StackSize(ST* st)
{
	assert(st);  // 断言，判断st 是否为 NULL

	return st->top;
	/*
	* 因为是数组，从0 下标开始，但是计数是要 +1 的
	* 而top 刚好就是下一个元素的位置与 计数值相同
	*/
}



/*
* 判断该栈是否为空栈
*/
bool StackEmpty(ST* st)
{
	return 0 == st->top;  
	/*
	* 判断栈是否为空
	* 当栈顶 top 为 0 是：表示栈没有一个数据，为空栈，返回 false (0)
	* 当栈顶 top 不是 0 时，栈不为空，返回 true (1);
	*/
}
