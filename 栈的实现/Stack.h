#pragma once

#include<stdio.h>
#include<assert.h>  // 断言
#include<stdlib.h>  
#include<stdbool.h>   // bool 布尔值

#define OCE 2             // 扩容的倍数
typedef int STDatatype;   // 定义该栈中的数据类型


/*
* 创建栈的结构体
*/
typedef struct Stack
{
	STDatatype* data;     //动态顺序表开辟空间
	int top;              // 栈顶的标识
	int copacity;         // 栈容量

}ST;


/* 函数，变量的声明 extern
* 声明是不会开辟空间的，所以声明可以多次
* 而定义是会开辟空间的，所以定义只能有一次
* 开辟空间不是目的，存放数据才是目的
* 因为只有开辟了空间，才可以存放数据
* 
*/
 
extern void StackInit(ST* st);                     // 初始化栈
extern void StackDestory(ST* st);                  // 销毁栈
extern void StackPush(ST* st, STDatatype x);       // 栈顶入栈操作
extern void StackPop(ST* st);                      // 栈顶出栈操作
extern STDatatype StackTop(ST* st);                // 取栈顶元素
extern int StackSize(ST* st);                      // 求栈中存在的数据个数
extern bool StackEmpty(ST* st);                    // 判断栈是否为空
extern void TestStack(ST* st);                           // 对于栈的测试

