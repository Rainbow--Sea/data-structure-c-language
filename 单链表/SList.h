#pragma once

#include<stdio.h>
#include<stdlib.h>

typedef int STLDataType;          // 类型的的别名的使用，方便后面的维护，修改，比如但我们把数据改为 flost的类型的时候
 
struct SListNode                  // 创建单链表的结构体
{
	STLDataType data;             // 单链表的数值 
	struct SListNode* next;       // 单链表的节点，注意：这里是指针地址的引用
};

typedef struct SListNode SLTNode; // 重新命名这个结构体的 别名。


///*注意下面这个是和上面那个的不同*/
//struct SListNode
//{
//	SLDataType data;
//	struct SListNode next; 
//	/*注意不可以：这样写的，这个代表的是：
//	结构体中套娃，嵌套了一个结构体，结构体中又嵌套了一个
//	结构体，这样无限嵌套结构体下去，就没有头了，你就不知道其大小了，sizeof()
//	*/
//};

// 注意：声明使用上 extern 关键字，代码好风格；
extern void SListPrint(SLTNode* phend);                        // 打印单链表
extern void SListPushBack(SLTNode** pphead, STLDataType num);  // 单链表的尾插法
extern void SListPushFront(SLTNode** pphead, STLDataType num); // 单链表的头插法
extern void SListPopFront(SLTNode** pphead);                   // 单链表的头删法
extern void SListPopBack(SLTNode** pphead);                    // 单链表的尾删法；
extern void SListInsert(SLTNode** pphead, STLDataType x, STLDataType num); // 单链表中数值为x的节点后插入 num数值
extern void SListErase(SLTNode** pphead, STLDataType num);     // 删除单链表中数值为 num 的节点
extern void SListErase_2(SLTNode** pphead, STLDataType num);   // 删除单链表中数值为 num 的节点
extern void SListSeek(SLTNode* phead, STLDataType num);        // 查找单链表中是否存在该数值的节点