#define  _CRT_SECURE_NO_WARNINGS  1

#include"SList.h"  // 注意：这是自定义的头文件，与书名号括起来的的头文件，
                   // 的查询方式是不一样的，


// 开辟节点空间
static SLTNode* BuySListNode(STLDataType num)  // staitc 修饰的方法只能在该文件中使用，
{                                              // 实质上是改变了其方法的作用域
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	newnode->data = num; 
	newnode->next = NULL;

	return newnode;
}


static SLTNode* SListFind(SLTNode* phead, STLDataType num)
{
	while (phead != NULL)              //地址不为空，存在节点
	{
		if (phead->data == num)
		{
			return phead;              // 找到返回地址
		}
		phead = phead->next;           // 移动指针；

	}

	return NULL;                       // 不存在，返回空（NULL）

}


void SListPrint(SLTNode* phend)     // 打印单链表的数据
{
	SLTNode* cur = phend;           // 用 cur 指针来代替头指针的访问，尽量不要移动头指针
	while (cur != NULL)             // 当节点的为 NULL的时候，就表示后面没有数据了
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n"); 
}


/*这里注意：形参和实参的传值的，改变传递地址，而只是一级指针的
的地址，所以注意它，接受的是二级指针，保存一级指针的地址，*/
void SListPushBack(SLTNode** pphead, STLDataType num)    // 单链表的尾插法
{
	SLTNode* newnode = BuySListNode(num);
	
	if (*pphead == NULL)         // 当插入的是第一个节点的时候，我们直接，用头指针指向该的位置地址
	{
		*pphead = newnode;
	}
	else                         // 当插入的不是第一个节点的时候，我们找到最后一个节点NULL
	{
		SLTNode* tail = *pphead;    // 同样使用一个变量，来代替头指针的访问，我们尽量不要头指针
		while (tail->next != NULL)  // 找到最后一个节点的位置 
		{
			tail = tail->next;      // 移动指针；
		}
		// 当跳出循环，找到了,插入
		tail->next = newnode;
	}
}


void SListPushFront(SLTNode** pphead, STLDataType num)  // 单链表的头插法
{
	SLTNode* newnode = BuySListNode(num);               // 创建节点空间
	newnode->next = *pphead;                            // 交互
	(*pphead) = newnode;
}


void SListPopFront(SLTNode** pphead)    // 单链表的头删法
{
	SLTNode* next = (*pphead)->next;    // 注意：不可以率先释放头指针的空间的，不然，你就丢失
	                                    // 个整个链表了。
	free(*pphead);                      // 释放空间，
	*pphead = next;
}


void SListPopBack(SLTNode** pphead)     // 单链表的尾删法
{
	/*1. 就是空的时候
	  2. 只有一个节点
	  3. 多个节点，主要就是要找到倒数第二个节点的位置*/

	if (*pphead == NULL)
	{
		return;
	}
	else if( (*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{ // 前后指针，主要就是为了，定位到倒数第二个节点的位置
		SLTNode* prev = NULL;
		SLTNode* tail = (*pphead);

		while (tail->next != NULL)
		{
			prev = tail;
			tail = tail->next; // 移动指针

		}
		// 跳出循环，找到了，最后一个节点
		free(tail); 
		tail = NULL;
		// 把倒数第二个的节点置为 NULL;
		prev->next = NULL;
	}

}


// 单链表 pow 位置的插入法
void SListInsert(SLTNode** pphead,STLDataType x, STLDataType num)
{
	SLTNode* pos = SListFind(*pphead, x);      // 判断找寻该数值的节点，
	if (pos)                       // 该数值的节点存在，在实行插入操作
	{
		if (pos == *pphead)             // 插入位置位于头部，使用复用，头插法
		{
			SListPushFront(pphead, num);   // 复用，头插法
		}
		else
		{    // 主要：找到该数值的，前一个节点的位置，进行交互
			SLTNode* newnode = BuySListNode(num);    // 创建节点
			SLTNode* prev = *pphead;        
			while (prev->next != pos)
			{
				prev = prev->next;                // 移动指针；
			}

			prev->next = newnode;
			newnode->next = pos;
		}
	}
	
}


void SListErase(SLTNode** pphead, STLDataType num)     // 删除数值为 num的节点
{
	SLTNode* pos = SListFind(*pphead, num);            // 找寻是否存在该数值的节点
	if (pos)                 // 存在才，进行删除操作
	{
		if (pos == *pphead)
		{
			// 头删法 复用
			SListPopFront(pphead); // 注意这里的参数是二级指针
		}
		else
		{
			// 前后指针 ，主要是为了；找到该数值的前面的节点，后进行交互
			SLTNode* prev = *pphead;
			SLTNode* tail = *pphead;
			while (tail != pos)
			{
				prev = tail;
				tail = tail->next;
			}
			prev->next = tail->next;
			free(pos);  // 释放空间
			tail = NULL;
			pos = NULL;  // 手动置为空（NULL）
		}
	}
}



 // 单链表中删除该数值位置的节点的 第二种方法，其中不同的就是找该数值，前的节点的位置的方法不同，而已 
void SListErase_2(SLTNode** pphead, STLDataType num)
{
	SLTNode* pos = SListFind(*pphead, num);      // 同样判断该数值的位置是否存在
	if (pos)           // 存在才，执行操作，
	{
		if (pos == *pphead)    // 位于头节点 ，复用
		{
			SListPopFront(pphead);  // 复用，头删法
		}
		else  // 找到该数值的前个位置的节点
		{
			SLTNode* prev = *pphead;
			while (prev->next != pos)
			{
				prev = prev->next;
			}
			
			prev ->next = pos->next;
			free(pos);  // 释放空间
			pos = NULL; // 手动置为空
			prev = NULL;

		}
	}
}

 
void SListSeek(SLTNode* phead, STLDataType num)   // 查找该数值是否存在
{
	SLTNode* tail = SListFind(phead, num);   // 判断数值节点是否存在，
	if (tail == NULL)
	{
		printf("该数值的节点不存在！\n");
	}
	else
	{
		printf("%d：该数值的节点存在\n",tail->data);
	}
}