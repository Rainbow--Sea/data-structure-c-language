#define  _CRT_SECURE_NO_WARNINGS  1

#include"SList.h"

void test1()
{
	SLTNode* plist = NULL;         // 创建头节点
	SListPushBack(&plist, 0);      // 单链表的尾插法
	SListPushBack(&plist, 1);      // 单链表的尾插法
	SListPushBack(&plist, 2);      // 单链表的尾插法
	SListPrint(plist);             // 单链表的打印
	SListPushFront(&plist, 10);    // 单链表单位头插法
	SListPushFront(&plist, 100);   // 单链表单位头插法
	SListPushFront(&plist, 1000);  // 单链表单位头插法
	SListPrint(plist);             // 单链表的打印
}

void test2()
{
	SLTNode* plist = NULL;         // 创建头节点
	SListPushBack(&plist, 0);      // 单链表的尾插法
	SListPushBack(&plist, 1);      // 单链表的尾插法
	SListPushBack(&plist, 2);      // 单链表的尾插法
	SListPopFront(&plist);         // 单链表单位头插法
	SListPrint(plist);             // 单链表的打印
	SListPushFront(&plist, 10);    // 单链表的头插法
	SListPushFront(&plist, 100);   // 单链表的头插法；
	SListPushFront(&plist, 1000);  // 单链表的头插法；
	SListPopFront(&plist);         // 单链表的头删法；
	SListPopFront(&plist);         // 单链表的头删法
	SListPopFront(&plist);         // 单链表的头删法
	SListPopFront(&plist);         // 单链表的头删法
	SListPopFront(&plist);         // 单链表的头删法

	SListPrint(plist);             // 单链表的打印
}

void test3()
{
	SLTNode* plist = NULL;         // 创建头节点
	SListPushBack(&plist, 0);      // 单链表的尾插法
	SListPushBack(&plist, 1);      // 单链表的尾插法
	SListPushBack(&plist, 2);      // 单链表的尾插法
	SListPrint(plist);             // 单链表的打印   
	SListPopBack(&plist);          // 单链表的尾删法
	SListPrint(plist);             // 单链表的打印   

}

void test4()
{
	SLTNode* plist = NULL;         // 创建头节点
	SListPushBack(&plist, 0);      // 单链表的尾插法
	SListPushBack(&plist, 1);      // 单链表的尾插法
	SListPushBack(&plist, 2);      // 单链表的尾插法
	SListPrint(plist);             // 单链表的打印 
	SListInsert(&plist, 0, 99);    // 单链表中在pos数值前插入数值节点
	SListInsert(&plist, 1, 10);    // 单链表中在pos数值前插入数值节点
	SListInsert(&plist, 2, 99);    // 单链表中在pos数值前插入数值节点
	SListInsert(&plist, 3,99);     // 单链表中在pos数值前插入数值节点
	SListPrint(plist);             // 单链表的打印 
	printf("********************\n");
	SListErase_2(&plist, 0);       // 单链表中删除pos数值的节点
	SListErase_2(&plist, 2);       // 单链表中删除pos数值的节点
	SListErase_2(&plist, 10);      // 单链表中删除pos数值的节点
	SListPrint(plist);             // 单链表的打印 
	SListPrint(plist);             // 单链表的打印 
	SListSeek(plist, 0);           // 单链表中删除pos数值的节点
	SListSeek(plist, 1);           // 单链表中删除pos数值的节点

}

int main()
{
	test1();
	test2();
	test3();
	printf("*********************\n");
	test4();
	return 0;
}