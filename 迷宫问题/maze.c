#define  _CRT_SECURE_NO_WARNINGS  1

#include"Stack.h"




ST path;       // 定义全局栈


// 打印迷宫数组
void printMaze(int** maze, int N, int M)    
{       
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			printf("%d", maze[i][j]);
		}
		printf("\n");
	}
	printf("\n");

}



// 输出栈里面的保存的路径
/*
	* 栈是： 后进的先出去，而我们要的是从起点 ———> 终点的一路的坐标
	* 因为我们最后入栈的是终点,栈的原理：后进先出,先从栈取出来的是终点坐标,不合题意
	* 所以我们： 将原来栈的保存的坐标，导入到另外一个栈中,从而将终点坐标放入到栈底了。
	* 这样我们将 拷贝的临时栈中的坐标内容打印出来,就是从起点 ——> 终点的一路的坐标路线。
*/


void printPath(ST* ps)
{
	ST tmp;    // 定义一个临时拷贝的栈,用于将原栈终点坐标,放入到 tmp 栈的栈底位置,

	StackInit(&tmp);   // 初始化 临时栈

	// 将原来栈的坐标内容 拷贝到 临时栈中(终点作为栈底)
	while (!StackEmpty(ps))  
	{
		StackPush(&tmp, StackTop(ps)); // 将原栈的栈顶坐标 ，入栈到tmp临时栈中
		StackPop(ps);  // 出栈,取栈顶元素后,需要出栈,不然无法取到栈后面的数据
	}

	// 
	while (!StackEmpty(&tmp))
	{
		PT top = StackTop(&tmp);    // 取出临时 tmp 栈中的栈顶坐标内容
		printf("(%d,%d)\n", top.x, top.y);  // 打印坐标
		StackPop(&tmp);             // 出栈,取栈顶要与出栈一起,不然无法取到栈后面的数据
	}

	StackDestory(&tmp);    // 销毁临时栈
}



// 判断当前迷宫中的坐标是否存在越界/障碍物, 
bool lsPass(int** mace, int N, int M, PT pos)
{
	if ((pos.x >= 0 && pos.x < N)
		&& (pos.y >= 0 && pos.y < M)   // 没有越界
		&& (mace[pos.x][pos.y] == 0))  // 不是障碍物
	{
		return true;
	}
	else
	{
		return false;
	}

}



// 坐标的上下左右的移动
bool getMazePath(int** maze, int N, int M, PT cur)
{
	StackPush(&path, cur);    // path 全局栈,cur 表示的是当前位置的坐标位置, 存入栈中

	// 判断该坐标位置是否达到终点位置
	if (cur.x == N - 1 && cur.y == M - 1)
	{
		return true;
	}

	PT next;  // 下一个临时坐标,用于保存当前(cur) 的坐标用于后面的移动
	maze[cur.x][cur.y] = 2; // 标记走过的坐标,防止往回走,形成死循环
 
	
	// 上走
	next = cur;    //next = cur;  
	next.x = next.x - 1;               // next.x = next.x - 1;
    if (lsPass(maze, N, M, next))    // 判断当前上走后的坐标位置(越界/障碍物),可否继续
	{
		if (getMazePath(maze, N, M, next))   // 可以继续,递归当前上走后的坐标位置,后的上下左右
		{
			return true;
		}
	}

	
	// 下走
	next = cur;
	next.x = next.x + 1;

	if (lsPass(maze, N, M, next))    // 判断当前下走后的坐标位置(越界/障碍物),可否继续
	{
		if (getMazePath(maze, N, M, next))  // 可以继续,递归当前下后的坐标位置,后的上下左右
		{
			return true;    // 到达终点返回
		}
	}


	// 向左走
	next = cur;
	next.y = next.y - 1; 

	if (lsPass(maze, N, M, next))  // 判断当前左走后的坐标位置(越界/障碍物),可否继续
	{
		if (getMazePath(maze, N, M, next)) // 可以继续,递归当前左走后的坐标位置上下左右
		{
			return true;
		}
	}


	// 向右走
	next = cur;
	next.y = next.y + 1;

	if (lsPass(maze, N, M, next))   // 判断当前右走后的坐标位置(越界/障碍物),可否继续
	{
		if (getMazePath(maze, N, M, next))  // 可以继续,递归当前右走后的坐标位置上下左右
		{
			return true;
		}
	}

	StackPop(&path);    // 将保存在栈中不可以走的坐标位置,出栈掉

	return false;
}



int main()
{
	 int N = 0;
	 int M = 0;

	     
	 while (scanf("%d%d", &N, &M) != EOF) // 输入二维数组的长宽, EOF 不等于 -1 无限输入
	 {
		 // int arr[N][M] 这样是不行的 在数组里面中的常量必须是，常量值  1,2,3
	 // 所以我们需要使用动态开辟二维数组：
	 // 使用指针数组,重点是指针, 指针数组用于存放指针的地址,

		 int** maze = (int**)malloc(sizeof(int*) * N);
		 // int** 双指针类型, 指针数组,数组元素是 指针的地址
		 //  malloc 返回堆区上开辟的空间的首地址, (int**) 强制转换为双指针,因为我们
		 // malloc 开辟类型是 (int*) 指针的地址，自然要使用 双指针才能保存到 指针的地址
		 // sizeof(int*)  该指针类型的大小, * 

		 // 判断堆区上空间开辟是否成功
		 if (NULL == maze)
		 {
			 perror("maze malloc error");  // 错误提醒
			 exit(-1);                 // 程序非正常结束
		 }

		 for (int i = 0; i < N; i++)
		 {
			 maze[i] = (int*)malloc(sizeof(int) * M);     // 堆区上开辟空间

			  //判断堆区上空间开辟是否成功
			 if (NULL == maze[i])
			 {
				 perror("maze[i] malloc error");
				 exit(-1);
			 }
		 }


		 // 迷宫构造的输入:
		 for (int i = 0; i < N; i++)
		 {
			 for (int j = 0; j < M; j++)
			 {
				 scanf("%d", &maze[i][j]);
			 }
		 }

		 StackInit(&path);  // 初始化 path全局栈
		 PT entry = { 0,0 };   // 定义初始坐标;为 0,0 结构体可以使用花括号全部初始赋值

		 if (getMazePath(maze, N, M, entry))
		 {
			 printPath(&path);    // 打印路线坐标
		 }
		 else
		 {
			 printf("没有找到通路\n");
		 }

		 StackDestory(&path);   // 销毁全局栈;
		 /*
		 * 注意我们这里是通过动态开辟的二维数组(指针数组),
		 * 要先将存放到指针数组中的指针的地址的空间先释放掉,再释放指针数组本身的地址空间
		 * 如果先释放了指针数组本身的地址空间的话,存放在指针数组中的指针的地址就无法再找到了,成了野指针了
		 * 更是无从释放了.
		 */
		 for (int i = 0; i < N; i++)  // 释放指针数组中存放的指针的地址
		 {
			 free(maze[i]);
		 }

		 free(maze);
		 maze = NULL;

	 }
	 
	return 0;
}